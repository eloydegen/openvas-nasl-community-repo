# OpenVAS NASL scripts

The regular database is only available as an rsync mirror and all other mirrors seemed outdated. Since it's all the GNU GPL, the scripts (written in the Nessus Attack Scripting Language) are mirrored here. Warning: browsing the scripts directory might slow down your browser, there are a lot of files.

There are two kinds of files in the script folder: `.nasl` scripts that are used for detecting vulnerabilities and `.inc`, which are include headers that can be used in the `.nasl` files.

Various procols have been implemented in `.inc` files, such as:
* SSL/TLS: `ssl_funcs.inc`
* HTTP: `http_func.inc`
* SIP: `sip.inc`
* RDP: `rdp.inc`
* MySQL: `mysql.inc`

# References 
* [The NASL2 reference guide](http://michel.arboi.free.fr/nasl2ref/nasl2_reference.pdf)
