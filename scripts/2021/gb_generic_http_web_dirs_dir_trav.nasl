# Copyright (C) 2021 Greenbone Networks GmbH
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.117574");
  script_version("2021-10-08T11:57:01+0000");
  script_tag(name:"last_modification", value:"2021-10-08 11:57:01 +0000 (Fri, 08 Oct 2021)");
  script_tag(name:"creation_date", value:"2021-07-22 12:59:06 +0000 (Thu, 22 Jul 2021)");
  script_tag(name:"cvss_base", value:"7.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:N/A:N");
  script_tag(name:"severity_vector", value:"CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N");
  script_tag(name:"severity_origin", value:"NVD");
  script_tag(name:"severity_date", value:"2018-11-07 15:43:00 +0000 (Wed, 07 Nov 2018)");

  script_cve_id("CVE-2014-3744", "CVE-2015-3337", "CVE-2017-1000028", "CVE-2017-14849", "CVE-2017-16877",
                "CVE-2017-6190", "CVE-2018-10822", "CVE-2018-1271", "CVE-2018-16288", "CVE-2018-16836",
                "CVE-2018-3714", "CVE-2018-3760", "CVE-2019-12314", "CVE-2019-14322", "CVE-2019-3799",
                "CVE-2020-35736", "CVE-2020-5405", "CVE-2021-23241", "CVE-2021-3223", "CVE-2021-40960",
                "CVE-2021-41773", "CVE-2021-42013");

  script_name("Generic HTTP Directory Traversal (HTTP Web Dirs Check)");
  script_category(ACT_ATTACK);
  script_copyright("Copyright (C) 2021 Greenbone Networks GmbH");
  script_family("Web application abuses");
  script_dependencies("find_service.nasl", "no404.nasl", "webmirror.nasl", "DDI_Directory_Scanner.nasl", "os_detection.nasl", "global_settings.nasl");
  script_require_ports("Services/www", 80);
  script_exclude_keys("Settings/disable_cgi_scanning", "global_settings/disable_generic_webapp_scanning");

  script_xref(name:"URL", value:"https://owasp.org/www-community/attacks/Path_Traversal");

  script_tag(name:"summary", value:"Generic check for HTTP directory traversal vulnerabilities on
  each HTTP directory.

  NOTE: Please enable 'Enable generic web application scanning' within the VT 'Global variable
  settings' (OID: 1.3.6.1.4.1.25623.1.0.12288) if you want to run this script.");

  script_tag(name:"impact", value:"Successfully exploiting this issue may allow an attacker to
  access paths and directories that should normally not be accessible by a user. This can result in
  effects ranging from disclosure of confidential information to arbitrary code execution.");

  script_tag(name:"affected", value:"The following products are known to be affected by the pattern
  checked in this VT:

  - CVE-2014-3744: st module for Node.js

  - CVE-2015-3337: Elasticsearch

  - CVE-2017-1000028: Oracle GlassFish Server

  - CVE-2017-6190 and CVE-2018-10822: D-Link Routers

  - CVE-2017-14849: Node.js

  - CVE-2017-16877: ZEIT Next.js

  - CVE-2018-1271: Spring MVC

  - CVE-2018-16288: LG SuperSign CMS

  - CVE-2018-16836: Rubedo

  - CVE-2018-3714: node-srv node module

  - CVE-2018-3760: Ruby on Rails

  - CVE-2019-12314: Deltek Maconomy

  - CVE-2019-14322: Pallets Werkzeug

  - CVE-2019-3799 and CVE-2020-5405: Spring Cloud Config

  - CVE-2020-35736: Gate One

  - CVE-2021-23241: MERCUSYS Mercury X18G

  - CVE-2021-3223: Node RED Dashboard

  - CVE-2021-40960: Galera WebTemplate

  - CVE-2021-41773 and CVE-2021-42013: Apache HTTP Server

  Other products might be affected as well.");

  script_tag(name:"vuldetect", value:"Sends crafted HTTP requests to the each found directory of the
  remote web server and checks the response.");

  script_tag(name:"solution", value:"Contact the vendor for a solution.");

  script_tag(name:"qod_type", value:"remote_vul");
  script_tag(name:"solution_type", value:"Mitigation");

  script_timeout(900);

  exit(0);
}

# nb: We also don't want to run if optimize_test is set to "no"
if( get_kb_item( "global_settings/disable_generic_webapp_scanning" ) )
  exit( 0 );

include("misc_func.inc");
include("host_details.inc");
include("os_func.inc");
include("http_func.inc");
include("http_keepalive.inc");
include("port_service_func.inc");
include("list_array_func.inc");

depth = get_kb_item( "global_settings/dir_traversal_depth" );
# nb: "" was added here to catch the (normally quite unlikely) case that the file is accessible
# via e.g. http://example.com/foo/etc/passwd
traversals = traversal_pattern( extra_pattern_list:make_list( "" ), depth:depth );
files = traversal_files();
count = 0;
max_count = 3;
suffixes = make_list(
  "",
  "%23vt/test", # Spring Cloud Config flaw (CVE-2020-5410) but other environments / technologies might be affected as well
  "%00" ); # PHP < 5.3.4 but other environments / technologies might be affected as well

prefixes = make_list(
  "",
  "c:" ); # Pallets Werkzeug (/base_import/static/c:/windows/win.ini, CVE-2019-14322) but other environments / technologies might be affected as well

port = http_get_port( default:80 );

# nb: If adding dirs here also add them to the related DDI_Directory_Scanner entries
# which have a prepended reference to this VT.
dirs = make_list_unique(
  # MERCUSYS Mercury X18G
  "/loginLess",
  # Gate One
  "/downloads",
  # st module for Node.js
  "/public",
  # Node.js and Spring MVC
  "/static",
  # Spring MVC
  "/spring-mvc-showcase/resources",
  # ZEIT Next.js
  "/_next",
  # LG SuperSign CMS
  "/signEzUI/playlist/edit/upload",
  # node-srv node module
  "/node_modules",
  # Node RED Dashboard
  "/ui_base/js",
  # Elasticsearch
  "/_plugin/head",
  # Oracle GlassFish Server
  "/theme/META-INF",
  # Rubedo
  "/theme/default/img",
  # Pallets Werkzeug
  "/base_import/static",
  "/web/static",
  "/base/static",
  # Deltek Maconomy
  "/cgi-bin/Maconomy/MaconomyWS.macx1.W_MCS",
  # D-Link Routers
  "/uir",
  # Galera WebTemplate (nb: folder from the PoC looks like a specific dir on a specific setup so
  # so a few different ones are checked)
  "/GallerySite/filesrc/fotoilan/388/middle/",
  "/GallerySite/filesrc/",
  "/GallerySite/",
  # Apache HTTP Server
  "/cgi-bin",
  # Ruby on Rails
  "/assets/file:",
  # nb: No need to add these two to the dir scanner:
  "/test/pathtraversal/master", # Spring Cloud Config
  "/a/b/", # Spring Cloud Config
  http_cgi_dirs( port:port ) );

foreach dir( dirs ) {

  if( dir == "/" )
    continue; # nb: Already checked in 2017/gb_generic_http_web_root_dir_trav.nasl

  dir_vuln = FALSE; # nb: Used later to only report each dir only once
  foreach traversal( traversals ) {
    foreach pattern( keys( files ) ) {
      file = files[pattern];
      foreach suffix( suffixes ) {
        foreach prefix( prefixes ) {
          url = dir + "/" + prefix + traversal + file + suffix;
          req = http_get( port:port, item:url );
          res = http_keepalive_send_recv( port:port, data:req );
          if( egrep( pattern:pattern, string:res, icase:TRUE ) ) {
            count++;
            dir_vuln = TRUE;
            vuln += http_report_vuln_url( port:port, url:url ) + '\n\n';
            vuln += 'Request:\n' + chomp( req ) + '\n\nResponse:\n' + chomp( res ) + '\n\n\n';
            break; # Don't report multiple vulnerable pattern / suffixes / prefixes for the very same dir
          }
        }
        if( count >= max_count || dir_vuln )
          break; # nb: No need to continue with that much findings or with multiple vulnerable pattern / suffixes / prefixes for the very same dir
      }
      if( count >= max_count || dir_vuln )
        break;
    }
    if( count >= max_count || dir_vuln )
      break;
  }

  if( count >= max_count )
    break;
}

if( vuln ) {
  report = 'The following affected URL(s) were found (limited to ' + max_count + ' results):\n\n' + chomp( vuln );
  security_message( port:port, data:report );
}

exit( 0 );