###############################################################################
# OpenVAS Vulnerability Test
#
# HP System Health Application and Command Line Utilities Version Detection (Linux)
#
# Authors:
# Madhuri D <dmadhuri@secpod.com>
#
# Copyright:
# Copyright (C) 2012 Greenbone Networks GmbH
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# (or any later version), as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
###############################################################################

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.802770");
  script_version("2021-10-12T14:25:54+0000");
  script_tag(name:"cvss_base", value:"0.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:N");
  script_tag(name:"last_modification", value:"2021-10-12 14:25:54 +0000 (Tue, 12 Oct 2021)");
  script_tag(name:"creation_date", value:"2012-05-08 13:05:57 +0530 (Tue, 08 May 2012)");
  script_name("HP System Health Application and Command Line Utilities Detection (Linux/Unix SSH Login)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2012 Greenbone Networks GmbH");
  script_family("Product detection");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/rpms");

  script_tag(name:"summary", value:"SSH login-based detection of the HP System Health Application
  and Command Line Utilities.");

  script_tag(name:"qod_type", value:"package");

  exit(0);
}

include("cpe.inc");
include("host_details.inc");

rpms = get_kb_item("ssh/login/rpms");
if(!rpms || rpms !~ "hp-health")
  exit(0);

version = eregmatch(pattern:"hp-health.?([0-9.]+)", string:rpms);
if(!version[1])
  exit(0);

path = "/opt/hp/hp-health/";

set_kb_item(name:"HP/Health/CLU", value:version[1]);
cpe = build_cpe(value:version[1], exp:"^([0-9.]+)", base:"cpe:/a:hp:system_health_application_and_command_line_utilities:");
if(!cpe)
  cpe = "cpe:/a:hp:system_health_application_and_command_line_utilities";

register_product(cpe:cpe, location:path, port:0, service:"ssh-login");

log_message(data:build_detection_report(app:"HP System Health Application and Command Line Utilities",
                                        version:version[1],
                                        install:path,
                                        cpe:cpe,
                                        concluded:"RPM package query: " + version[0]),
            port:0);

exit(0);