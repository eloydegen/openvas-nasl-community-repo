# Copyright (C) 2010 Greenbone Networks GmbH
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.100610");
  script_version("2022-01-11T10:28:11+0000");
  script_tag(name:"last_modification", value:"2022-01-11 10:28:11 +0000 (Tue, 11 Jan 2022)");
  script_tag(name:"creation_date", value:"2010-04-28 14:05:27 +0200 (Wed, 28 Apr 2010)");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:N/A:N");

  script_cve_id("CVE-2010-0738", "CVE-2010-1428", "CVE-2010-1429");

  script_tag(name:"qod_type", value:"remote_vul");

  script_tag(name:"solution_type", value:"VendorFix");

  script_name("JBoss Enterprise Application Platform Multiple Vulnerabilities");

  script_category(ACT_ATTACK);

  script_copyright("Copyright (C) 2010 Greenbone Networks GmbH");
  script_family("Web Servers");
  script_dependencies("gb_jboss_http_detect.nasl");
  script_require_ports("Services/www", 8080);
  script_mandatory_keys("jboss/detected");

  script_tag(name:"summary", value:"JBoss Enterprise Application Platform is prone to multiple
  vulnerabilities, including an information-disclosure issue and multiple authentication-bypass
  issues.");

  script_tag(name:"vuldetect", value:"Sends a crafted HTTP PUT request and checks the response.");

  script_tag(name:"impact", value:"An attacker can exploit these issues to bypass certain security
  restrictions to obtain sensitive information or gain unauthorized access to the application.");

  script_tag(name:"solution", value:"Updates are available. Please see the references for details.");

  script_xref(name:"URL", value:"http://www.securityfocus.com/bid/39710");

  exit(0);
}

include("http_func.inc");
include("http_keepalive.inc");
include("misc_func.inc");

if( ! port = get_kb_item( "jboss/port" ) )
  exit( 0 );

url = "/jmx-console";
buf = http_get_cache( item:url, port:port );
if( ! buf || buf =~ "^HTTP/1\.[01] [23]00" )
  exit( 0 );

url = "/jmx-console/checkJNDI.jsp";
host = http_host_name( port:port );

req = http_post_put_req( port:port, url:url, method:"PUT" );
res = http_keepalive_send_recv( port:port, data:req );

if( res =~ "^HTTP/1\.[01] 200" && "JNDI Check</title>" >< res && "JNDI Checking for host" >< res ) {
  report = http_report_vuln_url( port:port, url:url );
  security_message( port:port, data:report );
  exit( 0 );
}

exit( 99 );
