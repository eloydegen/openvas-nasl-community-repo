# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1991-1 (squid/squid3)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2010 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66808");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2010-02-10 21:51:26 +0100 (Wed, 10 Feb 2010)");
  script_cve_id("CVE-2009-2855", "CVE-2010-0308");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 1991-1 (squid/squid3)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2010 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201991-1");
  script_tag(name:"insight", value:"Two denial of service vulnerabilities have been discovered in
squid and squid3, a web proxy. The Common Vulnerabilities and Exposures
project identifies the following problems:

CVE-2009-2855

Bastian Blank discovered that it is possible to cause a denial of
service via a crafted auth header with certain comma delimiters.

CVE-2010-0308

Tomas Hoger discovered that it is possible to cause a denial of service
via invalid DNS header-only packets.


For the stable distribution (lenny), these problems have been fixed in
version 2.7.STABLE3-4.1lenny1 of the squid package and version
3.0.STABLE8-3+lenny3 of the squid3 package.

For the oldstable distribution (etch), these problems have been fixed in
version 2.6.5-6etch5 of the squid package and version 3.0.PRE5-5+etch2
of the squid3 package.

For the testing distribution (squeeze) and the unstable distribution
(sid), these problems will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your squid/squid3 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to squid/squid3
announced via advisory DSA 1991-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"squid3-common", ver:"3.0.PRE5-5+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid-common", ver:"2.6.5-6etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid3-cgi", ver:"3.0.PRE5-5+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid3-client", ver:"3.0.PRE5-5+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid3", ver:"3.0.PRE5-5+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid-cgi", ver:"2.6.5-6etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid", ver:"2.6.5-6etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squidclient", ver:"2.6.5-6etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid-common", ver:"2.7.STABLE3-4.1lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid3-common", ver:"3.0.STABLE8-3+lenny3", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid", ver:"2.7.STABLE3-4.1lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squidclient", ver:"3.0.STABLE8-3+lenny3", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid3", ver:"3.0.STABLE8-3+lenny3", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid-cgi", ver:"2.7.STABLE3-4.1lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"squid3-cgi", ver:"3.0.STABLE8-3+lenny3", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}