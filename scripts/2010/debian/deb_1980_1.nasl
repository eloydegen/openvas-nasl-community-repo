# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1980-1 (ircd-hybrid/ircd-ratbox)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2010 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66773");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2010-02-01 18:25:19 +0100 (Mon, 01 Feb 2010)");
  script_cve_id("CVE-2009-4016", "CVE-2010-0300");
  script_tag(name:"cvss_base", value:"6.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1980-1 (ircd-hybrid/ircd-ratbox)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2010 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB5");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201980-1");
  script_tag(name:"insight", value:"David Leadbeater discovered an integer underflow that could be triggered
via the LINKS command and can lead to a denial of service or the
execution of arbitrary code (CVE-2009-4016). This issue affects both,
ircd-hybrid and ircd-ratbox.

It was discovered that the ratbox IRC server is prone to a denial of
service attack via the HELP command. The ircd-hybrid package is not
vulnerable to this issue (CVE-2010-0300).


For the stable distribution (lenny), this problem has been fixed in
version 1:7.2.2.dfsg.2-4+lenny1 of the ircd-hybrid package and in
version 2.2.8.dfsg-2+lenny1 of ircd-ratbox.

Due to a bug in the archive software it was not possible to release the
fix for the oldstable distribution (etch) simultaneously. The packages
will be released as version 7.2.2.dfsg.2-3+etch1 once they become
available.

For the testing distribution (squeeze) and the unstable distribution
(sid), this problem will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your ircd-hybrid/ircd-ratbox packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to ircd-hybrid/ircd-ratbox
announced via advisory DSA 1980-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"hybrid-dev", ver:"7.2.2.dfsg.2-4+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ircd-ratbox", ver:"2.2.8.dfsg-2+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ircd-ratbox-dbg", ver:"2.2.8.dfsg-2+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ircd-hybrid", ver:"7.2.2.dfsg.2-4+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}