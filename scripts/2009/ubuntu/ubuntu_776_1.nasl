###############################################################################
# OpenVAS Vulnerability Test
#
# Auto-generated from advisory USN-776-1 (kvm)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
###############################################################################

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.63998");
  script_version("2022-01-21T08:56:07+0000");
  script_tag(name:"last_modification", value:"2022-01-21 08:56:07 +0000 (Fri, 21 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-05-20 00:17:15 +0200 (Wed, 20 May 2009)");
  script_cve_id("CVE-2008-1945", "CVE-2008-2004", "CVE-2008-2382", "CVE-2008-4539", "CVE-2008-5714");
  script_tag(name:"cvss_base", value:"7.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:N/A:N");
  script_name("Ubuntu USN-776-1 (kvm)");
  script_category(ACT_GATHER_INFO);
  script_xref(name:"URL", value:"http://www.ubuntu.com/usn/usn-776-1/");
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Ubuntu Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/ubuntu_linux", "ssh/login/packages", re:"ssh/login/release=UBUNTU(8\.04 LTS|8\.10)");
  script_tag(name:"insight", value:"Avi Kivity discovered that KVM did not correctly handle certain disk
formats.  A local attacker could attach a malicious partition that
would allow the guest VM to read files on the VM host. (CVE-2008-1945,
CVE-2008-2004)

Alfredo Ortega discovered that KVM's VNC protocol handler did not
correctly validate certain messages.  A remote attacker could send
specially crafted VNC messages that would cause KVM to consume CPU
resources, leading to a denial of service. (CVE-2008-2382)

Jan Niehusmann discovered that KVM's Cirrus VGA implementation over VNC
did not correctly handle certain bitblt operations.  A local attacker
could exploit this flaw to potentially execute arbitrary code on the VM
host or crash KVM, leading to a denial of service. (CVE-2008-4539)

It was discovered that KVM's VNC password checks did not use the correct
length.  A remote attacker could exploit this flaw to cause KVM to crash,
leading to a denial of service. (CVE-2008-5714)");
  script_tag(name:"summary", value:"The remote host is missing an update to kvm
announced via advisory USN-776-1.");
  script_tag(name:"solution", value:"The problem can be corrected by upgrading your system to the
 following package versions:

Ubuntu 8.04 LTS:
  kvm                             1:62+dfsg-0ubuntu8.1

Ubuntu 8.10:
  kvm                             1:72+dfsg-1ubuntu6.1

After a standard system upgrade you need to restart all KVM VMs to effect
the necessary changes.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=USN-776-1");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if ((res = isdpkgvuln(pkg:"kvm-source", ver:"62+dfsg-0ubuntu8.1", rls:"UBUNTU8.04 LTS")) != NULL) {
    report += res;
}
if ((res = isdpkgvuln(pkg:"kvm", ver:"62+dfsg-0ubuntu8.1", rls:"UBUNTU8.04 LTS")) != NULL) {
    report += res;
}
if ((res = isdpkgvuln(pkg:"kvm-source", ver:"72+dfsg-1ubuntu6.1", rls:"UBUNTU8.10")) != NULL) {
    report += res;
}
if ((res = isdpkgvuln(pkg:"kvm", ver:"72+dfsg-1ubuntu6.1", rls:"UBUNTU8.10")) != NULL) {
    report += res;
}

if (report != "") {
    security_message(data:report);
} else if (__pkg_match) {
    exit(99);
}
