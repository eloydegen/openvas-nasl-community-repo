# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1924-1 (mahara)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc. http://www.securityspace.com
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66204");
  script_version("2022-01-19T13:57:34+0000");
  script_tag(name:"last_modification", value:"2022-01-19 13:57:34 +0000 (Wed, 19 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-11-11 15:56:44 +0100 (Wed, 11 Nov 2009)");
  script_cve_id("CVE-2009-3298", "CVE-2009-3299");
  script_tag(name:"cvss_base", value:"6.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:S/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1924-1 (mahara)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB5");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201924-1");
  script_tag(name:"insight", value:"Two vulnerabilities have been discovered in, an electronic portfolio,
weblog, and resume builder.  The Common Vulnerabilities and Exposures
project identifies the following problems:

CVE-2009-3298

Ruslan Kabalin discovered an issue with resetting passwords, which could
lead to a privilege escalation of an institutional administrator
account.

CVE-2009-3299

Sven Vetsch discovered a cross-site scripting vulnerability via the
resume fields.


For the stable distribution (lenny), these problems have been fixed in
version 1.0.4-4+lenny4.

The oldstable distribution (etch) does not contain mahara.

For the testing distribution (squeeze) and the unstable distribution
(sid), this problem will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your mahara packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to mahara
announced via advisory DSA 1924-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"mahara-apache2", ver:"1.0.4-4+lenny4", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mahara", ver:"1.0.4-4+lenny4", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}