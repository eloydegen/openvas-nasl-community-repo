# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1750-1 (libpng)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.63682");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-03-31 19:20:21 +0200 (Tue, 31 Mar 2009)");
  script_cve_id("CVE-2007-2445", "CVE-2007-5269", "CVE-2008-1382", "CVE-2008-5907", "CVE-2008-6218", "CVE-2009-0040");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1750-1 (libpng)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201750-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in libpng, a library for
reading and writing PNG files. The Common Vulnerabilities and
Exposures project identifies the following problems:

The png_handle_tRNS function allows attackers to cause a denial of
service (application crash) via a grayscale PNG image with a bad tRNS
chunk CRC value. (CVE-2007-2445)

Certain chunk handlers allow attackers to cause a denial of service
(crash) via crafted pCAL, sCAL, tEXt, iTXt, and ztXT chunking in PNG
images, which trigger out-of-bounds read operations. (CVE-2007-5269)

libpng allows context-dependent attackers to cause a denial of service
(crash) and possibly execute arbitrary code via a PNG file with zero
length unknown chunks, which trigger an access of uninitialized
memory. (CVE-2008-1382)

The png_check_keyword might allow context-dependent attackers to set the
value of an arbitrary memory location to zero via vectors involving
creation of crafted PNG files with keywords. (CVE-2008-5907)

A memory leak in the png_handle_tEXt function allows context-dependent
attackers to cause a denial of service (memory exhaustion) via a crafted
PNG file. (CVE-2008-6218)

libpng allows context-dependent attackers to cause a denial of service
(application crash) or possibly execute arbitrary code via a crafted PNG
file that triggers a free of an uninitialized pointer in (1) the
png_read_png function, (2) pCAL chunk handling, or (3) setup of 16-bit
gamma tables. (CVE-2009-0040)

For the old stable distribution (etch), these problems have been fixed
in version1.2.15~beta5-1+etch2.

For the stable distribution (lenny), these problems have been fixed in
version 1.2.27-2+lenny2.  (Only CVE-2008-5907, CVE-2008-5907 and
CVE-2009-0040 affect the stable distribution.)

For the unstable distribution (sid), these problems have been fixed in
version 1.2.35-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your libpng packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to libpng
announced via advisory DSA 1750-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libpng3", ver:"1.2.15~beta5-1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpng12-dev", ver:"1.2.15~beta5-1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpng12-0", ver:"1.2.15~beta5-1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpng3", ver:"1.2.27-2+lenny2", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpng12-dev", ver:"1.2.27-2+lenny2", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpng12-0", ver:"1.2.27-2+lenny2", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}