# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1826-1 (eggdrop)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.64378");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-07-15 04:21:35 +0200 (Wed, 15 Jul 2009)");
  script_cve_id("CVE-2007-2807", "CVE-2009-1789");
  script_tag(name:"cvss_base", value:"6.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1826-1 (eggdrop)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201826-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in eggdrop, an advanced IRC
robot. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2007-2807

It was discovered that eggdrop is vulnerable to a buffer overflow, which
could result in a remote user executing arbitrary code. The previous DSA
(DSA-1448-1) did not fix the issue correctly.

CVE-2009-1789

It was discovered that eggdrop is vulnerable to a denial of service
attack, that allows remote attackers to cause a crash via a crafted
PRIVMSG.

For the stable distribution (lenny), these problems have been fixed in
version 1.6.19-1.1+lenny1.

For the old stable distribution (etch), these problems have been fixed in
version 1.6.18-1etch2.

For the unstable distribution (sid), this problem has been fixed in
version 1.6.19-1.2");

  script_tag(name:"solution", value:"We recommend that you upgrade your eggdrop package.");
  script_tag(name:"summary", value:"The remote host is missing an update to eggdrop
announced via advisory DSA 1826-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"eggdrop-data", ver:"1.6.18-1etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"eggdrop", ver:"1.6.18-1etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"eggdrop-data", ver:"1.6.19-1.1+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"eggdrop", ver:"1.6.19-1.1+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}