# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1912-2 (advi)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66099");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-10-27 01:37:56 +0100 (Tue, 27 Oct 2009)");
  script_cve_id("CVE-2009-3296", "CVE-2009-2660");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1912-2 (advi)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB5");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201912-2");
  script_tag(name:"insight", value:"Due to the fact that advi, an active DVI previewer and presenter,
statically links against camlimages it was necessary to rebuilt it in
order to incorporate the latest security fixes for camlimages, which
could lead to integer overflows via specially crafted TIFF files
(CVE-2009-3296) or GIFF and JPEG images (CVE-2009-2660).


For the stable distribution (lenny), these problems have been fixed in
version 1.6.0-13+lenny2.

Due to a bug in the archive system, the fix for the oldstable
distribution (etch) cannot be released at the same time. These problems
will be fixed in version 1.6.0-12+etch2, once it is available.

For the testing distribution (squeeze) and the unstable distribution
(sid), these problems have been fixed in version 1.6.0-14+b1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your advi package.");
  script_tag(name:"summary", value:"The remote host is missing an update to advi
announced via advisory DSA 1912-2.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"advi-examples", ver:"1.6.0-13+lenny2", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"advi", ver:"1.6.0-13+lenny2", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}