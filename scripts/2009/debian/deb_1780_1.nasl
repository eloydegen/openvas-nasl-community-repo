# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1780-1 (libdbd-pg-perl)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.63933");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-05-05 16:00:35 +0200 (Tue, 05 May 2009)");
  script_cve_id("CVE-2009-0663", "CVE-2009-1341");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1780-1 (libdbd-pg-perl)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201780-1");
  script_tag(name:"insight", value:"Two vulnerabilities have been discovered in libdbd-pg-perl, the DBI
driver module for PostgreSQL database access (DBD::Pg).

CVE-2009-0663

A heap-based buffer overflow may allow attackers to execute arbitrary
code through applications which read rows from the database using the
pg_getline and getline functions.  (More common retrieval methods,
such as selectall_arrayref and fetchrow_array, are not affected.)

CVE-2009-1341

A memory leak in the routine which unquotes BYTEA values returned from
the database allows attackers to cause a denial of service.

For the old stable distribution (etch), these problems have been fixed
in version 1.49-2+etch1.

For the stable distribution (lenny) and the unstable distribution (sid),
these problems have been fixed in version 2.1.3-1 before the release of
lenny.");

  script_tag(name:"solution", value:"We recommend that you upgrade your libdbd-pg-perl package.");
  script_tag(name:"summary", value:"The remote host is missing an update to libdbd-pg-perl
announced via advisory DSA 1780-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libdbd-pg-perl", ver:"1.49-2+etch1", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}