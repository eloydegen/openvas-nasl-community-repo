# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1852-1 (fetchmail)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.64631");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-08-17 16:54:45 +0200 (Mon, 17 Aug 2009)");
  script_cve_id("CVE-2009-2666");
  script_tag(name:"cvss_base", value:"6.4");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:N");
  script_name("Debian Security Advisory DSA 1852-1 (fetchmail)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201852-1");
  script_tag(name:"insight", value:"It was discovered that fetchmail, a full-featured remote mail retrieval
and forwarding utility, is vulnerable to the Null Prefix Attacks Against
SSL/TLS Certificates recently published at the Blackhat conference.
This allows an attacker to perform undetected man-in-the-middle attacks
via a crafted ITU-T X.509 certificate with an injected null byte in the
subjectAltName or Common Name fields.

Note, as a fetchmail user you should always use strict certificate
validation through either these option combinations:
sslcertck ssl sslproto ssl3    (for service on SSL-wrapped ports)
or
sslcertck sslproto tls1        (for STARTTLS-based services)


For the oldstable distribution (etch), this problem has been fixed in
version 6.3.6-1etch2.

For the stable distribution (lenny), this problem has been fixed in
version 6.3.9~rc2-4+lenny1.

For the testing distribution (squeeze), this problem will be fixed soon.

For the unstable distribution (sid), this problem has been fixed in
version 6.3.9~rc2-6.");

  script_tag(name:"solution", value:"We recommend that you upgrade your fetchmail packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to fetchmail
announced via advisory DSA 1852-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"fetchmailconf", ver:"6.3.6-1etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"fetchmail", ver:"6.3.6-1etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"fetchmailconf", ver:"6.3.9~rc2-4+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"fetchmail", ver:"6.3.9~rc2-4+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}