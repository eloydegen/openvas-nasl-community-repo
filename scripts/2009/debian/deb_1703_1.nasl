# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1703-1 (bind9)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.63150");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-01-13 22:38:32 +0100 (Tue, 13 Jan 2009)");
  script_cve_id("CVE-2009-0025");
  script_tag(name:"cvss_base", value:"6.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1703-1 (bind9)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201703-1");
  script_tag(name:"insight", value:"It was discovered that BIND, an implementation of the DNS protocol
suite, does not properly check the result of an OpenSSL function which
is used to verify DSA cryptographic signatures.  As a result,
incorrect DNS resource records in zones protected by DNSSEC could be
accepted as genuine.

For the stable distribution (etch), this problem has been fixed in
version 9.3.4-2etch4.

For the unstable distribution (sid) and the testing distribution
(lenny), this problem will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your BIND packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to bind9
announced via advisory DSA 1703-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"bind9-doc", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"lwresd", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libbind9-0", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisccfg1", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisccc0", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisc11", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"liblwres9", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libdns22", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9-host", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libbind-dev", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"dnsutils", ver:"9.3.4-2etch4", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}