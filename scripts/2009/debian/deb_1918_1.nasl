# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1918-1 (phpmyadmin)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66102");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-10-27 01:37:56 +0100 (Tue, 27 Oct 2009)");
  script_cve_id("CVE-2009-3696", "CVE-2009-3697");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1918-1 (phpmyadmin)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201918-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in phpMyAdmin, a tool
to administer MySQL over the web. The Common Vulnerabilities and Exposures
project identifies the following problems:

CVE-2009-3696

Cross-site scripting (XSS) vulnerability allows remote attackers to
inject arbitrary web script or HTML via a crafted MySQL table name.

CVE-2009-3697

SQL injection vulnerability in the PDF schema generator functionality
allows remote attackers to execute arbitrary SQL commands. This issue
does not apply to the version in Debian 4.0 Etch.

Additionally, extra fortification has been added for the web based setup.php
script. Although the shipped web server configuration should ensure that
this script is protected, in practice this turned out not always to be the
case. The config.inc.php file is not writable anymore by the webserver user
anymore. See README.Debian for details on how to enable the setup.php
script if and when you need it.


For the old stable distribution (etch), these problems have been fixed in
version 2.9.1.1-13.

For the stable distribution (lenny), these problems have been fixed in
version 2.11.8.1-5+lenny3.

For the unstable distribution (sid), these problems have been fixed in
version 3.2.2.1-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your phpmyadmin package.");
  script_tag(name:"summary", value:"The remote host is missing an update to phpmyadmin
announced via advisory DSA 1918-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"phpmyadmin", ver:"2.9.1.1-13", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"phpmyadmin", ver:"2.11.8.1-5+lenny3", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}