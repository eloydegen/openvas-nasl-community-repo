# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1881-1 (cyrus-imapd-2.2)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.64864");
  script_cve_id("CVE-2009-2632");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-09-15 22:46:32 +0200 (Tue, 15 Sep 2009)");
  script_tag(name:"cvss_base", value:"4.4");
  script_tag(name:"cvss_base_vector", value:"AV:L/AC:M/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1881-1 (cyrus-imapd-2.2)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201881-1");
  script_tag(name:"insight", value:"It was discovered that the SIEVE component of cyrus-imapd, a highly scalable
enterprise mail system, is vulnerable to a buffer overflow when processing
SIEVE scripts.  Due to incorrect use of the sizeof() operator an attacker is
able to pass a negative length to snprintf() calls resulting in large positive
values due to integer conversion.  This causes a buffer overflow which can be
used to elevate privileges to the cyrus system user.  An attacker who is able
to install SIEVE scripts executed by the server is therefore able to read and
modify arbitrary email messages on the system.


For the oldstable distribution (etch), this problem has been fixed in
version 2.2.13-10+etch2.

For the stable distribution (lenny), this problem has been fixed in
version 2.2.13-14+lenny1.

For the testing (squeeze) and unstable (sid) distribution, this problem
will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your cyrus-imapd-2.2 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to cyrus-imapd-2.2
announced via advisory DSA 1881-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"cyrus-doc-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-admin-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-murder-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-imapd-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-clients-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcyrus-imap-perl22", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-nntpd-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-dev-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-pop3d-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-common-2.2", ver:"2.2.13-10+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-admin-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-doc-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-nntpd-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-dev-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcyrus-imap-perl22", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-pop3d-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-common-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-imapd-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-murder-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cyrus-clients-2.2", ver:"2.2.13-14+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}