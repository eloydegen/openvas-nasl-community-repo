# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1901-1 (mediawiki1.7)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.65007");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-10-06 02:49:40 +0200 (Tue, 06 Oct 2009)");
  script_cve_id("CVE-2008-5249", "CVE-2008-5250", "CVE-2008-5252", "CVE-2009-0737");
  script_tag(name:"cvss_base", value:"5.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:N/I:P/A:P");
  script_name("Debian Security Advisory DSA 1901-1 (mediawiki1.7)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201901-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in mediawiki1.7, a website engine
for collaborative work. The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2008-5249

David Remahl discovered that mediawiki1.7 is prone to a cross-site scripting attack.

CVE-2008-5250

David Remahl discovered that mediawiki1.7, when Internet Explorer is used and
uploads are enabled, or an SVG scripting browser is used and SVG uploads are
enabled, allows remote authenticated users to inject arbitrary web script or
HTML by editing a wiki page.

CVE-2008-5252

David Remahl discovered that mediawiki1.7 is prone to a cross-site request
forgery vulnerability in the Special:Import feature.

CVE-2009-0737

It was discovered that mediawiki1.7 is prone to a cross-site scripting attack in
the web-based installer.


For the oldstable distribution (etch), these problems have been fixed in version
1.7.1-9etch1 for mediawiki1.7, and mediawiki is not affected (it is a
metapackage for mediawiki1.7).

The stable (lenny) distribution does not include mediawiki1.7, and these
problems have been fixed in version 1:1.12.0-2lenny3 for mediawiki which was
already included in the lenny release.

The unstable (sid) and testing (squeeze) distributions do not
include mediawiki1.7, and these problems have been fixed in version 1:1.14.0-1
for mediawiki.");

  script_tag(name:"solution", value:"We recommend that you upgrade your mediawiki1.7 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to mediawiki1.7
announced via advisory DSA 1901-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"mediawiki1.7", ver:"1.7.1-9etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mediawiki1.7-math", ver:"1.7.1-9etch1", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}