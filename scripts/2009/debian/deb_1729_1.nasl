# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1729-1 (gst-plugins-bad0.10)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.63496");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-03-07 21:47:03 +0100 (Sat, 07 Mar 2009)");
  script_cve_id("CVE-2009-0386", "CVE-2009-0387", "CVE-2009-0397");
  script_tag(name:"cvss_base", value:"9.3");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1729-1 (gst-plugins-bad0.10)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201729-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been found in gst-plugins-bad0.10, a
collection of various GStreamer plugins. The Common Vulnerabilities and
Exposures project identifies the following problems:

CVE-2009-0386

Tobias Klein discovered a buffer overflow in the quicktime stream
demuxer (qtdemux), which could potentially lead to the execution of
arbitrary code via crafted .mov files.

CVE-2009-0387

Tobias Klein discovered an array index error in the quicktime stream
demuxer (qtdemux), which could potentially lead to the execution of
arbitrary code via crafted .mov files.

CVE-2009-0397

Tobias Klein discovered a buffer overflow in the quicktime stream
demuxer (qtdemux) similar to the issue reported in CVE-2009-0386, which
could also lead to the execution of arbitrary code via crafted .mov
files.


For the stable distribution (lenny), these problems have been fixed in
version 0.10.8-4.1~lenny1 of gst-plugins-good0.10, since the affected
plugin has been moved there. The fix was already included in the lenny
release.

For the oldstable distribution (etch), these problems have been fixed in
version 0.10.3-3.1+etch1.

For the unstable distribution (sid) and the testing distribution
(squeeze), these problems have been fixed in version 0.10.8-4.1 of
gst-plugins-good0.10.");
  script_tag(name:"summary", value:"The remote host is missing an update to gst-plugins-bad0.10
announced via advisory DSA 1729-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution", value:"Please install the updated package(s).");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"gstreamer0.10-plugins-bad", ver:"0.10.3-3.1+etch1", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}