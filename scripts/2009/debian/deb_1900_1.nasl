# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1900-1 (postgresql-7.4, postgresql-8.1, postgresql-8.3, postgresql-8.4)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.65005");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-10-06 02:49:40 +0200 (Tue, 06 Oct 2009)");
  script_cve_id("CVE-2009-3229", "CVE-2009-3230", "CVE-2009-3231");
  script_tag(name:"cvss_base", value:"6.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1900-1 (postgresql-7.4, postgresql-8.1, postgresql-8.3, postgresql-8.4)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201900-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in PostgreSQL, an SQL
database system.  The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2009-3229

Authenticated users can shut down the backend server by re-LOAD-ing
libraries in $libdir/plugins, if any libraries are present there.
(The old stable distribution (etch) is not affected by this issue.)

CVE-2009-3230

Authenticated non-superusers can gain database superuser privileges if
they can create functions and tables due to incorrect execution of
functions in functional indexes.

CVE-2009-3231

If PostgreSQL is configured with LDAP authentication, and the LDAP
configuration allows anonymous binds, it is possible for a user to
authenticate themselves with an empty password.  (The old stable
distribution (etch) is not affected by this issue.)

In addition, this update contains reliability improvements which do
not target security issues.

For the old stable distribution (etch), these problems have been fixed
in version 7.4.26-0etch1 of the postgresql-7.4 source package, and
version 8.1.18-0etch1 of the postgresql-8.1 source package.

For the stable distribution (lenny), these problems have been fixed in
version 8.3.8-0lenny1 of the postgresql-8.3 source package.

For the unstable distribution (sid), these problems have been fixed in
version 8.3.8-1 of the postgresql-8.3 source package, and version
8.4.1-1 of the postgresql-8.4 source package.");

  script_tag(name:"solution", value:"We recommend that you upgrade your PostgreSQL packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to postgresql-7.4, postgresql-8.1, postgresql-8.3, postgresql-8.4
announced via advisory DSA 1900-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"postgresql-server-dev-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-doc-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-doc-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-contrib-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-plperl-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpq4", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpq-dev", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-plpython-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libecpg5", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libecpg-compat2", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-plperl-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-contrib-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libecpg-dev", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-plpython-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpgtypes2", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-pltcl-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-server-dev-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-client-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-7.4", ver:"7.4.26-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-pltcl-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-client-8.1", ver:"8.1.18-0etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-doc-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-contrib", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-client", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-doc", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-pltcl-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpq-dev", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-contrib-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libecpg-compat3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpq5", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libpgtypes3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libecpg6", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-client-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-server-dev-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-plperl-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libecpg-dev", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-plpython-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"postgresql-8.3", ver:"8.3.8-0lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}