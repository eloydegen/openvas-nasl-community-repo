# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1818-1 (gforge)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.64253");
  script_cve_id("CVE-2009-4069", "CVE-2009-4070");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-06-23 15:49:15 +0200 (Tue, 23 Jun 2009)");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1818-1 (gforge)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201818-1");
  script_tag(name:"insight", value:"Laurent Almeras and Guillaume Smet have discovered a possible SQL
injection vulnerability and cross-site scripting vulnerabilities in
gforge, a collaborative development tool. Due to insufficient input
sanitising, it was possible to inject arbitrary SQL statements and use
several parameters to conduct cross-site scripting attacks.

For the stable distribution (lenny), these problem have been fixed in
version 4.7~rc2-7lenny1.

The oldstable distribution (etch), these problems have been fixed in
version 4.5.14-22etch11.

For the testing distribution (squeeze), these problems will be fixed
soon.

For the unstable distribution (sid), these problems have been fixed in
version 4.7.3-2.");

  script_tag(name:"solution", value:"We recommend that you upgrade your gforge packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to gforge
announced via advisory DSA 1818-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"gforge-common", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-db-postgresql", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-shell-ldap", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-ftp-proftpd", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-mta-exim4", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-mta-postfix", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-ldap-openldap", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-mta-courier", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-mta-exim", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-shell-postgresql", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-web-apache", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-lists-mailman", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-dns-bind9", ver:"4.5.14-22etch11", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-dns-bind9", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-plugin-mediawiki", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-ftp-proftpd", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-plugin-scmcvs", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-db-postgresql", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-mta-exim4", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-mta-postfix", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-shell-postgresql", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-web-apache", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-mta-courier", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-lists-mailman", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-common", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-plugin-scmsvn", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"gforge-web-apache2", ver:"4.7~rc2-7lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}