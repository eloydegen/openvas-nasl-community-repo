# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1923-1 (libhtml-parser-perl)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66147");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-11-11 15:56:44 +0100 (Wed, 11 Nov 2009)");
  script_cve_id("CVE-2009-3627");
  script_tag(name:"cvss_base", value:"4.3");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 1923-1 (libhtml-parser-perl)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201923-1");
  script_tag(name:"insight", value:"A denial of service vulnerability has been found in libhtml-parser-perl,
a collection of modules to parse HTML in text documents which is used by
several other projects like e.g. SpamAssassin.

Mark Martinec discovered that the decode_entities() function will get stuck
in an infinite loop when parsing certain HTML entities with invalid UTF-8
characters.  An attacker can use this to perform denial of service attacks
by submitting crafted HTML to an application using this functionality.


For the oldstable distribution (etch), this problem has been fixed in
version 3.55-1+etch1.

For the stable distribution (lenny), this problem has been fixed in
version 3.56-1+lenny1.

For the testing (squeeze) and unstable (sid) distribution, this problem
will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your libhtml-parser-perl packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to libhtml-parser-perl
announced via advisory DSA 1923-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libhtml-parser-perl", ver:"3.55-1+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libhtml-parser-perl", ver:"3.56-1+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}