# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1799-1 (qemu)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.63995");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-05-20 00:17:15 +0200 (Wed, 20 May 2009)");
  script_cve_id("CVE-2008-0928", "CVE-2008-4539", "CVE-2008-1945");
  script_tag(name:"cvss_base", value:"7.2");
  script_tag(name:"cvss_base_vector", value:"AV:L/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1799-1 (qemu)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201799-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in the QEMU processor
emulator. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2008-0928

Ian Jackson discovered that range checks of file operations on
emulated disk devices were insufficiently enforced.

CVE-2008-1945

It was discovered that an error in the format auto detection of
removable media could lead to the disclosure of files in the
host system.

CVE-2008-4539

A buffer overflow has been found in the emulation of the Cirrus
graphics adaptor.


For the old stable distribution (etch), these problems have been fixed in
version 0.8.2-4etch3.

For the stable distribution (lenny), these problems have been fixed in
version 0.9.1-10lenny1.

For the unstable distribution (sid), these problems have been fixed in
version 0.9.1+svn20081101-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your qemu packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to qemu
announced via advisory DSA 1799-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"qemu", ver:"0.8.2-4etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qemu", ver:"0.9.1-10lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}