# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1850-1 (libmodplug)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.64562");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-08-17 16:54:45 +0200 (Mon, 17 Aug 2009)");
  script_cve_id("CVE-2009-1438", "CVE-2009-1513");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1850-1 (libmodplug)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201850-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in libmodplug, the shared
libraries for mod music based on ModPlug. The Common Vulnerabilities and
Exposures project identifies the following problems:

CVE-2009-1438

It was discovered that libmodplug is prone to an integer overflow when
processing a MED file with a crafted song comment or song name.

CVE-2009-1513

It was discovered that libmodplug is prone to a buffer overflow in the
PATinst function, when processing a long instrument name.


For the stable distribution (lenny), these problems have been fixed in
version 1:0.8.4-1+lenny1.

For the oldstable distribution (etch), these problems have been fixed in
version 1:0.7-5.2+etch1.

For the testing distribution (squeeze) and the unstable distribution
(sid), this problem has been fixed in version 1:0.8.7-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your libmodplug packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to libmodplug
announced via advisory DSA 1850-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libmodplug-dev", ver:"0.7-5.2+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmodplug0c2", ver:"0.7-5.2+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmodplug-dev", ver:"0.8.4-1+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmodplug0c2", ver:"0.8.4-1+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}