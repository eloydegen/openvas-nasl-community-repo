# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1955-1 (network-manager/network-manager-applet)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66593");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-12-30 21:58:43 +0100 (Wed, 30 Dec 2009)");
  script_cve_id("CVE-2009-0365");
  script_tag(name:"cvss_base", value:"4.6");
  script_tag(name:"cvss_base_vector", value:"AV:L/AC:L/Au:S/C:C/I:N/A:N");
  script_name("Debian Security Advisory DSA 1955-1 (network-manager/network-manager-applet)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201955-1");
  script_tag(name:"insight", value:"It was discovered that network-manager-applet, a network management
framework, lacks some dbus restriction rules, which allows local users
to obtain sensitive information.

If you have locally modified the /etc/dbus-1/system.d/nm-applet.conf
file, then please make sure that you merge the changes from this fix
when asked during upgrade.


For the stable distribution (lenny), this problem has been fixed in
version 0.6.6-4+lenny1 of network-manager-applet.

For the oldstable distribution (etch), this problem has been fixed in
version 0.6.4-6+etch1 of network-manager.

For the testing distribution (squeeze) and the unstable distribution
(sid), this problem has been fixed in version 0.7.0.99-1 of
network-manager-applet.");

  script_tag(name:"solution", value:"We recommend that you upgrade your network-manager and");
  script_tag(name:"summary", value:"The remote host is missing an update to network-manager/network-manager-applet
announced via advisory DSA 1955-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"network-manager-gnome", ver:"0.6.4-6+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"network-manager-dev", ver:"0.6.4-6+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"network-manager", ver:"0.6.4-6+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libnm-glib0", ver:"0.6.4-6+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libnm-util0", ver:"0.6.4-6+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libnm-glib-dev", ver:"0.6.4-6+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libnm-util-dev", ver:"0.6.4-6+etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"network-manager-gnome", ver:"0.6.6-4+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}