# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1899-1 (strongswan)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.65006");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-10-06 02:49:40 +0200 (Tue, 06 Oct 2009)");
  script_cve_id("CVE-2009-1957", "CVE-2009-1958", "CVE-2009-2185", "CVE-2009-2661");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 1899-1 (strongswan)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201899-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in strongswan, an
implementation of the IPSEC and IKE protocols.  The Common
Vulnerabilities and Exposures project identifies the following
problems:

CVE-2009-1957
CVE-2009-1958

The charon daemon can crash when processing certain crafted IKEv2
packets.  (The old stable distribution (etch) was not affected by
these two problems because it lacks IKEv2 support.)

CVE-2009-2185
CVE-2009-2661

The pluto daemon could crash when processing a crafted X.509
certificate.

For the old stable distribution (etch), these problems have been fixed
in version 2.8.0+dfsg-1+etch2.

For the stable distribution (lenny), these problems have been fixed in
version 4.2.4-5+lenny3.

For the unstable distribution (sid), these problems have been fixed in
version 4.3.2-1.1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your strongswan packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to strongswan
announced via advisory DSA 1899-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"strongswan", ver:"2.8.0+dfsg-1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"strongswan", ver:"4.2.4-5+lenny3", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}