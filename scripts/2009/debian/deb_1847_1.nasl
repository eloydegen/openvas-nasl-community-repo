# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1847-1 (bind9)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.64558");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-08-17 16:54:45 +0200 (Mon, 17 Aug 2009)");
  script_cve_id("CVE-2009-0696");
  script_tag(name:"cvss_base", value:"4.3");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 1847-1 (bind9)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201847-1");
  script_tag(name:"insight", value:"It was discovered that the BIND DNS server terminates when processing a
specially crafted dynamic DNS update.  This vulnerability affects all
BIND servers which serve at least one DNS zone authoritatively, as a
master, even if dynamic updates are not enabled.  The default Debian
configuration for resolvers includes several authoritative zones, too,
so resolvers are also affected by this issue unless these zones have
been removed.

For the old stable distribution (etch), this problem has been fixed in
version 9.3.4-2etch5.

For the stable distribution (lenny), this problem has been fixed in
version 9.5.1.dfsg.P3-1.

For the unstable distribution (sid), this problem has been fixed in
version 1:9.6.1.dfsg.P1-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your bind9 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to bind9
announced via advisory DSA 1847-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"bind9-doc", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisccc0", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libdns22", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libbind9-0", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9-host", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisc11", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"liblwres9", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisccfg1", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"lwresd", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libbind-dev", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"dnsutils", ver:"9.3.4-2etch5", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9-doc", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisc45", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libbind9-40", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"dnsutils", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisccfg40", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libisccc40", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9utils", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"bind9-host", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libdns45", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"liblwres40", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"lwresd", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libbind-dev", ver:"9.5.1.dfsg.P3-1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}