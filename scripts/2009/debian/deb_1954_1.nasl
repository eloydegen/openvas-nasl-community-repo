# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1954-1 (cacti)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2009 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# or at your option, GNU General Public License version 3,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.66592");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2009-12-30 21:58:43 +0100 (Wed, 30 Dec 2009)");
  script_cve_id("CVE-2007-3112", "CVE-2007-3113", "CVE-2009-4032", "CVE-2009-4112");
  script_tag(name:"cvss_base", value:"9.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:S/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1954-1 (cacti)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2009 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|5)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201954-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been found in cacti, a frontend to rrdtool
for monitoring systems and services. The Common Vulnerabilities and
Exposures project identifies the following problems:

CVE-2007-3112, CVE-2007-3113

It was discovered that cacti is prone to a denial of service via the
graph_height, graph_width, graph_start and graph_end parameters.
This issue only affects the oldstable (etch) version of cacti.

CVE-2009-4032

It was discovered that cacti is prone to several cross-site scripting
attacks via different vectors.

CVE-2009-4112

It has been discovered that cacti allows authenticated administrator
users to gain access to the host system by executing arbitrary commands
via the Data Input Method for the Linux - Get Memory Usage setting.

There is no fix for this issue at this stage. Upstream will implement a
whitelist policy to only allow certain safe commands. For the moment,
we recommend that such access is only given to trusted users and that
the options Data Input and User Administration are otherwise
deactivated.


For the oldstable distribution (etch), these problems have been fixed in
version 0.8.6i-3.6.

For the stable distribution (lenny), this problem has been fixed in
version 0.8.7b-2.1+lenny1.

For the testing distribution (squeeze), this problem will be fixed soon.

For the unstable distribution (sid), this problem has been fixed in
version 0.8.7e-1.1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your cacti packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to cacti
announced via advisory DSA 1954-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"cacti", ver:"0.8.6i-3.6", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cacti", ver:"0.8.7b-2.1+lenny1", rls:"DEB5")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if (__pkg_match) {
  exit(99);
}