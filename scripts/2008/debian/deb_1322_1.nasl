# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1322-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.58444");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:19:52 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2007-3390", "CVE-2007-3392", "CVE-2007-3393");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 1322-1 (wireshark)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201322-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in the Wireshark
network traffic analyzer, which may lead to denial of service. The Common
Vulnerabilities and Exposures project identifies the following problems:

CVE-2007-3390

Off-by-one overflows were discovered in the iSeries dissector.

CVE-2007-3392

The MMS and SSL dissectors could be forced into an infinite loop.

CVE-2007-3393

An off-by-one overflow was discovered in the DHCP/BOOTP dissector.

The oldstable distribution (sarge) is not affected by these problems.
(In Sarge Wireshark used to be called Ethereal).

For the stable distribution (etch) these problems have been fixed
in version 0.99.4-5.etch.0. Packages for the big endian MIPS architecture
are not yet available. They will be provided later.

For the unstable distribution (sid) these problems have been fixed in
version 0.99.6pre1-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your Wireshark packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to wireshark
announced via advisory DSA 1322-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"ethereal", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ethereal-common", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ethereal-dev", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"tethereal", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"tshark", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wireshark", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wireshark-common", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wireshark-dev", ver:"0.99.4-5.etch.0", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
