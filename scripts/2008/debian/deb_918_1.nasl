# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 918-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.55973");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:07:13 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2005-3347", "CVE-2005-3533");
  script_tag(name:"cvss_base", value:"7.2");
  script_tag(name:"cvss_base_vector", value:"AV:L/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 918-1 (osh)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.0|3\.1)");
  script_tag(name:"solution", value:"For the stable distribution (sarge) these problems have been fixed in
version 1.7-13sarge1.

For the unstable distribution (sid) these problems have been fixed in
version 1.7-15, however, the package has been removed entirely.

  We recommend that you upgrade your osh package.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20918-1");
  script_tag(name:"summary", value:"The remote host is missing an update to osh
announced via advisory DSA 918-1.

Several security related problems have been discovered in osh, the
operator's shell for executing defined programs in a privileged
environment.  The Common Vulnerabilities and Exposures project
identifies the following vulnerabilities:

CVE-2005-3347

Charles Stevenson discovered a bug in the substitution of
variables that allows a local attacker to open a root shell.

CVE-2005-3533

Solar Eclipse discovered a buffer overflow caused by the current
working directory plus a filename that could be used to execute
arbitrary code and e.g. open a root shell.

For the old stable distribution (woody) these problems have been fixed in
version 1.7-11woody2.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"osh", ver:"1.7-11woody2", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"osh", ver:"1.7-13sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
