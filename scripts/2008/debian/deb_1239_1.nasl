# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1239-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.57738");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:17:11 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2006-4244", "CVE-2006-4731", "CVE-2006-5872");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1239-1 (sql-ledger)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB3\.1");
  script_tag(name:"solution", value:"For the stable distribution (sarge) these problems have been fixed in
version 2.4.7-2sarge1.

For the upcoming stable distribution (etch) these problems have been
fixed in version 2.6.21-1.

For the unstable distribution (sid) these problems have been fixed in
version 2.6.21-1.

  We recommend that you upgrade your sql-ledger packages.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201239-1");
  script_tag(name:"summary", value:"The remote host is missing an update to sql-ledger
announced via advisory DSA 1239-1.

Several remote vulnerabilities have been discovered in SQL Ledger, a web
based double-entry accounting program, which may lead to the execution
of arbitrary code. The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2006-4244

Chris Travers discovered that the session management can be tricked
into hijacking existing sessions.

CVE-2006-4731

Chris Travers discovered that directory traversal vulnerabilities
can be exploited to execute arbitrary Perl code.

CVE-2006-5872

It was discovered that missing input sanitising allows execution of
arbitrary Perl code.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"sql-ledger", ver:"2.4.7-2sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
