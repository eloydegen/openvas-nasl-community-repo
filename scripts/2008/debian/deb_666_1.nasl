# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 666-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.53496");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 22:56:38 +0100 (Thu, 17 Jan 2008)");
  script_bugtraq_id(12437);
  script_cve_id("CVE-2005-0089");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 666-1 (python2.2)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB3\.0");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20666-1");
  script_tag(name:"insight", value:"The Python development team has discovered a flaw in their language
package.  The SimpleXMLRPCServer library module could permit remote
attackers unintended access to internals of the registered object or
its module or possibly other modules.  The flaw only affects Python
XML-RPC servers that use the register_instance() method to register an
object without a _dispatch() method.  Servers using only
register_function() are not affected.

For the stable distribution (woody) this problem has been fixed in
version 2.2.1-4.7.  No other version of Python in woody is affected.

For the testing (sarge) and unstable (sid) distributions the following
matrix explains which version will contain the correction in which
version:

testing                   unstable
Python 2.2     2.2.3-14                  2.2.3-14
Python 2.3     2.3.4-20               2.3.4+2.3.5c1-2
Python 2.4      2.4-5                     2.4-5");

  script_tag(name:"solution", value:"We recommend that you upgrade your Python packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to python2.2
announced via advisory DSA 666-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"idle-python2.2", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-doc", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-elisp", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-examples", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-dev", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-gdbm", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-mpz", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-tk", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python2.2-xmlbase", ver:"2.2.1-4.7", rls:"DEB3.0")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
