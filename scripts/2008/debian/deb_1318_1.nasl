# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1318-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.58424");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:19:52 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2005-2370", "CVE-2005-2448", "CVE-2007-1663", "CVE-2007-1664", "CVE-2007-1665");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 1318-1 (ekg)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.1|4)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201318-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in ekg, a console
Gadu Gadu client. The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2005-2370

It was discovered that memory alignment errors may allow remote
attackers to cause a denial of service on certain architectures
such as sparc. This only affects Debian Sarge.

CVE-2005-2448

It was discovered that several endianness errors may allow remote
attackers to cause a denial of service. This only affects
Debian Sarge.

CVE-2007-1663

It was discovered that a memory leak in handling image messages may
lead to denial of service. This only affects Debian Etch.

CVE-2007-1664

It was discovered that a null pointer deference in the token OCR code
may lead to denial of service. This only affects Debian Etch.

CVE-2007-1665

It was discovered that a memory leak in the token OCR code may lead
to denial of service. This only affects Debian Etch.

For the oldstable distribution (sarge) these problems have been fixed in
version 1.5+20050411-7. This updates lacks updated packages for the m68k
architecture. They will be provided later.

For the stable distribution (etch) these problems have been fixed
in version 1:1.7~rc2-1etch1.

For the unstable distribution (sid) these problems have been fixed in
version 1:1.7~rc2-2.");

  script_tag(name:"solution", value:"We recommend that you upgrade your ekg packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to ekg
announced via advisory DSA 1318-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"ekg", ver:"1.5+20050411-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgadu-dev", ver:"1.5+20050411-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgadu3", ver:"1.5+20050411-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ekg", ver:"1.7~rc2-1etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgadu-dev", ver:"1.7~rc2-1etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgadu3", ver:"1.7~rc2-1etch1", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
