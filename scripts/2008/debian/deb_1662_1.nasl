# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1662-1 (mysql-dfsg-5.0)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61852");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-11-19 16:52:57 +0100 (Wed, 19 Nov 2008)");
  script_cve_id("CVE-2008-4098", "CVE-2008-4097");
  script_tag(name:"cvss_base", value:"4.6");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:H/Au:S/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1662-1 (mysql-dfsg-5.0)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201662-1");
  script_tag(name:"insight", value:"A symlink traversal vulnerability was discovered in MySQL, a
relational database server.  The weakness could permit an attacker
having both CREATE TABLE access to a database and the ability to
execute shell commands on the database server to bypass MySQL access
controls, enabling them to write to tables in databases to which they
would not ordinarily have access.

The Common Vulnerabilities and Exposures project identifies this
vulnerability as CVE-2008-4098.  Note that a closely aligned issue,
identified as CVE-2008-4097, was prevented by the update announced in
DSA-1608-1.  This new update supersedes that fix and mitigates both
potential attack vectors.

For the stable distribution (etch), this problem has been fixed in
version 5.0.32-7etch8.");

  script_tag(name:"solution", value:"We recommend that you upgrade your mysql packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to mysql-dfsg-5.0
announced via advisory DSA 1662-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"mysql-server", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-common", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-client", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-client-5.0", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-server-5.0", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmysqlclient15-dev", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-server-4.1", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmysqlclient15off", ver:"5.0.32-7etch8", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
