# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1451-1 (mysql-dfsg-5.0)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.60106");
  script_version("2022-01-19T13:57:34+0000");
  script_tag(name:"last_modification", value:"2022-01-19 13:57:34 +0000 (Wed, 19 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:23:47 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2007-3781", "CVE-2007-5969", "CVE-2007-6304");
  script_tag(name:"cvss_base", value:"7.1");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:H/Au:S/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1451-1 (mysql-dfsg-5.0)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201451-1");
  script_tag(name:"insight", value:"Several local/remote vulnerabilities have been discovered in the MySQL
database server. The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2007-3781

It was discovered that privilege validation for the source table
of CREATE TABLE LIKE statements was insufficiently enforced, which
might lead to information disclosure. This is only exploitable by
authenticated users.

CVE-2007-5969

It was discovered that symbolic links were handled insecurely during
the creation of tables with DATA DIRECTORY or INDEX DIRECTORY
statements, which might lead to denial of service by overwriting
data. This is only exploitable by authenticated users.

CVE-2007-6304

It was discovered that queries to data in a FEDERATED table can
lead to a crash of the local database server, if the remote server
returns information with less columns than expected, resulting in
denial of service.

For the unstable distribution (sid), these problems have been fixed in
version 5.0.51-1.

For the stable distribution (etch), these problems have been fixed in
version 5.0.32-7etch4.

The old stable distribution (sarge) doesn't contain mysql-dfsg-5.0.");

  script_tag(name:"solution", value:"We recommend that you upgrade your mysql-dfsg-5.0 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to mysql-dfsg-5.0
announced via advisory DSA 1451-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"mysql-client", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-server", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-common", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmysqlclient15-dev", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmysqlclient15off", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-server-4.1", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-client-5.0", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mysql-server-5.0", ver:"5.0.32-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
