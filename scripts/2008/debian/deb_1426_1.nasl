# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1426-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.59960");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:23:47 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2007-3388", "CVE-2007-4137");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1426-1 (qt-x11-free)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.1|4)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201426-1");
  script_tag(name:"insight", value:"Several local/remote vulnerabilities have been discovered in the Qt GUI
Library. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2007-3388

Tim Brown and Dirk Mueller discovered several format string
vulnerabilities in the handling of error messages, which might lead
to the execution of arbitrary code.

CVE-2007-4137

Dirk Muller discovered an off-by-one buffer overflow in the Unicode
handling, which might lead to the execution of arbitrary code.

For the old stable distribution (sarge), these problems have been fixed
in version 3:3.3.4-3sarge3. Packages for m68k will be provided later.

For the stable distribution (etch), these problems have been fixed in
version 3:3.3.7-4etch1.

For the unstable distribution (sid), these problems have been fixed in
version 3:3.3.7-8.");

  script_tag(name:"solution", value:"We recommend that you upgrade your qt-x11-free packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to qt-x11-free
announced via advisory DSA 1426-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libqt3-i18n", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-doc", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-examples", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-mt-mysql", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-psql", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-dev-tools", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-compat-headers", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-dev-tools-compat", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-qtconfig", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt-dev", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-mysql", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-mt-sqlite", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-mt-psql", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-headers", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-designer", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-mt", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-mt-odbc", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-odbc", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-dev-tools-embedded", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-assistant", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-apps-dev", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-linguist", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-sqlite", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-dev", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-mt-ibase", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3c102-ibase", ver:"3.3.4-3sarge3", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-examples", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-doc", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-i18n", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-dev-tools-embedded", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt-odbc", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt-dev", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-dev-tools-compat", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-linguist", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-designer", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-headers", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt-psql", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-qtconfig", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-assistant", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt-sqlite", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-apps-dev", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt-x11-free-dbg", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt-mysql", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-compat-headers", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"qt3-dev-tools", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libqt3-mt-ibase", ver:"3.3.7-4etch1", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
