# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1583-1 (gnome-peercast)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61035");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-05-27 15:41:50 +0200 (Tue, 27 May 2008)");
  script_cve_id("CVE-2007-6454", "CVE-2008-2040");
  script_tag(name:"cvss_base", value:"10.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1583-1 (gnome-peercast)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201583-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in Gnome PeerCast,
the Gnome interface to PeerCast, a P2P audio and video streaming
server. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2007-6454

Luigi Auriemma discovered that PeerCast is vulnerable to a heap
overflow in the HTTP server code, which allows remote attackers to
cause a denial of service and possibly execute arbitrary code via a
long SOURCE request.

CVE-2008-2040

Nico Golde discovered that PeerCast, a P2P audio and video streaming
server, is vulnerable to a buffer overflow in the HTTP Basic
Authentication code, allowing a remote attacker to crash PeerCast or
execute arbitrary code.

For the stable distribution (etch), these problems have been fixed in
version 0.5.4-1.1etch0.

For the unstable distribution (sid), the first issue has been fixed in
0.5.4-1.2. The second issue will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your gnome-peercast package.");
  script_tag(name:"summary", value:"The remote host is missing an update to gnome-peercast
announced via advisory DSA 1583-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"gnome-peercast", ver:"0.5.4-1.1etch0", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
