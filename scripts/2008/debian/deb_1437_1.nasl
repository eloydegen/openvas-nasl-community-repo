# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1437-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc. http://www.securityspace.com
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.60069");
  script_version("2022-01-19T13:57:34+0000");
  script_tag(name:"last_modification", value:"2022-01-19 13:57:34 +0000 (Wed, 19 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:23:47 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2007-5849", "CVE-2007-6358");
  script_tag(name:"cvss_base", value:"9.3");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1437-1 (cupsys)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201437-1");
  script_tag(name:"insight", value:"Several local vulnerabilities have been discovered in the Common UNIX
Printing System. The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2007-5849

Wei Wang discovered that a buffer overflow in the SNMP backend
may lead to the execution of arbitrary code.

CVE-2007-6358

Elias Pipping discovered that insecure handling of a temporary
file in the pdftops.pl script may lead to local denial of service.
This vulnerability is not exploitable in the default configuration.

For the stable distribution (etch), these problems have been fixed in
version 1.2.7-4etch2.

The old stable distribution (sarge) is not affected by CVE-2007-5849.
The other issue doesn't warrant an update on it's own and has been
postponed.

For the unstable distribution (sid), these problems have been fixed in
version 1.3.5-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your cupsys packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to cupsys
announced via advisory DSA 1437-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libcupsys2-gnutls10", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-common", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-dbg", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsimage2", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsimage2-dev", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-client", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsys2-dev", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-bsd", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsys2", ver:"1.2.7-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
