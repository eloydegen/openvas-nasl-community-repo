# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1539-1 (mapserver)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.60784");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-04-21 20:40:14 +0200 (Mon, 21 Apr 2008)");
  script_cve_id("CVE-2007-4542", "CVE-2007-4629");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1539-1 (mapserver)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201539-1");
  script_tag(name:"insight", value:"Chris Schmidt and Daniel Morissette discovered two vulnerabilities
in mapserver, a development environment for spatial and mapping
applications.  The Common Vulnerabilities and Exposures project
identifies the following two problems:

CVE-2007-4542

Lack of input sanitizing and output escaping in the CGI
mapserver's template handling and error reporting routines leads
to cross-site scripting vulnerabilities.

CVE-2007-4629

Missing bounds checking in mapserver's template handling leads to
a stack-based buffer overrun vulnerability, allowing a remote
attacker to execute arbitrary code with the privileges of the CGI
or httpd user.

For the stable distribution (etch), these problems have been fixed in
version 4.10.0-5.1+etch2.

For the unstable distribution (sid), these problems have been fixed in
version 4.10.3-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your mapserver (4.10.0-5.1+etch2) package.");
  script_tag(name:"summary", value:"The remote host is missing an update to mapserver
announced via advisory DSA 1539-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"mapserver-doc", ver:"4.10.0-5.1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cgi-mapserver", ver:"4.10.0-5.1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php5-mapscript", ver:"4.10.0-5.1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mapserver-bin", ver:"4.10.0-5.1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mapscript", ver:"4.10.0-5.1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"python-mapscript", ver:"4.10.0-5.1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"perl-mapscript", ver:"4.10.0-5.1+etch2", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
