# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1525-1 (asterisk)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.60616");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-03-27 18:25:13 +0100 (Thu, 27 Mar 2008)");
  script_cve_id("CVE-2007-6430", "CVE-2008-1332", "CVE-2008-1333");
  script_tag(name:"cvss_base", value:"8.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:C/I:C/A:N");
  script_name("Debian Security Advisory DSA 1525-1 (asterisk)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201525-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in Asterisk, a free
software PBX and telephony toolkit. The Common Vulnerabilities and
Exposures project identifies the following problems:

CVE-2007-6430

Tilghman Lesher discovered that database-based registrations are
insufficiently validated. This only affects setups, which are
configured to run without a password and only host-based
authentication.

CVE-2008-1332

Jason Parker discovered that insufficient validation of From:
headers inside the SIP channel driver may lead to authentication
bypass and the potential external initiation of calls.

This update also fixes a format string vulnerability, which can only
be triggered through configuration files under control of the local
administrator. In later releases of Asterisk this issue is remotely
exploitable and tracked as CVE-2008-1333.

For the stable distribution (etch), these problems have been fixed in
version 1:1.2.13~dfsg-2etch3.

The status of the old stable distribution (sarge) is currently being
investigated. If affected, an update will be released through
security.debian.org.");

  script_tag(name:"solution", value:"We recommend that you upgrade your asterisk packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to asterisk
announced via advisory DSA 1525-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"asterisk-doc", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk-sounds-main", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk-config", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk-web-vmail", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk-dev", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk-bristuff", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk-h323", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"asterisk-classic", ver:"1.2.13~dfsg-2etch3", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
