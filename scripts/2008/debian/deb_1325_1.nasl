# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1325-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.58446");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:19:52 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2007-1002", "CVE-2007-3257");
  script_tag(name:"cvss_base", value:"6.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1325-1 (evolution)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(4|3\.1)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201325-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in Evolution, a
groupware suite with mail client and organizer. The Common Vulnerabilities
and Exposures project identifies the following problems:

CVE-2007-1002

Ulf Harnhammer discovered that a format string vulnerability in
the handling of shared calendars may allow the execution of arbitrary
code.

CVE-2007-3257

It was discovered that the IMAP code in the Evolution Data Server
performs insufficient sanitising of a value later used an array index,
which can lead to the execution of arbitrary code.

For the oldstable distribution (sarge) these problems have been fixed in
version 2.0.4-2sarge2. Packages for hppa, mips and powerpc are not yet
available. They will be provided later.

For the stable distribution (etch) these problems have been fixed
in version 2.6.3-6etch1. Packages for mips are not yet available. They
will be provided later.

For the unstable distribution (sid) these problems will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your evolution packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to evolution
announced via advisory DSA 1325-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"evolution-common", ver:"2.6.3-6etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"evolution", ver:"2.6.3-6etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"evolution-dbg", ver:"2.6.3-6etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"evolution-dev", ver:"2.6.3-6etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"evolution-plugins", ver:"2.6.3-6etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"evolution-plugins-experimental", ver:"2.6.3-6etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"evolution", ver:"2.0.4-2sarge2", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"evolution-dev", ver:"2.0.4-2sarge2", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
