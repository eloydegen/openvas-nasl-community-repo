# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1238-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.57737");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:17:11 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2006-6406", "CVE-2006-6481");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:P/A:N");
  script_name("Debian Security Advisory DSA 1238-1 (clamav)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB3\.1");
  script_tag(name:"solution", value:"For the stable distribution (sarge) these problems have been fixed in
version 0.84-2.sarge.13.

For the upcoming stable distribution (etch) these problems have been
fixed in version 0.88.7-1.

For the unstable distribution (sid) these problems have been fixed in
version 0.88.7-1.

  We recommend that you upgrade your clamav packages.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201238-1");
  script_tag(name:"summary", value:"The remote host is missing an update to clamav
announced via advisory DSA 1238-1.

Several remote vulnerabilities have been discovered in the Clam anti-virus
toolkit. The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2006-6406

Hendrik Weimer discovered that invalid characters in base64 encoded
data may lead to bypass of scanning mechanisms.

CVE-2006-6481

Hendrik Weimer discovered that deeply nested multipart/mime MIME
data may lead to denial of service.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"clamav-base", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"clamav-docs", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"clamav-testfiles", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"clamav", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"clamav-daemon", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"clamav-freshclam", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"clamav-milter", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libclamav-dev", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libclamav1", ver:"0.84-2.sarge.13", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
