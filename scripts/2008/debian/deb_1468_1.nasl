# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1468-1 (tomcat5.5)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.60212");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-31 16:11:48 +0100 (Thu, 31 Jan 2008)");
  script_cve_id("CVE-2008-0128", "CVE-2007-2450");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:N/A:N");
  script_name("Debian Security Advisory DSA 1468-1 (tomcat5.5)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201468-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in the Tomcat
servlet and JSP engine. The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2008-0128

Olaf Kock discovered that HTTPS encryption was insufficiently
enforced for single-sign-on cookies, which could result in
information disclosure.

CVE-2007-2450

It was discovered that the Manager and Host Manager web applications
performed insufficient input sanitising, which could lead to cross-
site scripting.

This update also adapts the tomcat5.5-webapps package to the tightened
JULI permissions introduced in the previous tomcat5.5 DSA. However, it
should be noted, that the tomcat5.5-webapps is for demonstration and
documentation purposes only and should not be used for production
systems.

For the unstable distribution (sid), these problems will be fixed soon.

For the stable distribution (etch), these problems have been fixed in
version 5.5.20-2etch2.

The old stable distribution (sarge) doesn't contain tomcat5.5.");

  script_tag(name:"solution", value:"We recommend that you upgrade your tomcat5.5 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to tomcat5.5
announced via advisory DSA 1468-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"tomcat5.5-admin", ver:"5.5.20-2etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libtomcat5.5-java", ver:"5.5.20-2etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"tomcat5.5", ver:"5.5.20-2etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"tomcat5.5-webapps", ver:"5.5.20-2etch2", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
