# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1296-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.58345");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:17:11 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2007-2509");
  script_tag(name:"cvss_base", value:"2.6");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:H/Au:N/C:N/I:P/A:N");
  script_name("Debian Security Advisory DSA 1296-1 (php4)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.1|4)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201296-1");
  script_tag(name:"insight", value:"It was discovered that the ftp extension of PHP, a server-side,
HTML-embedded scripting language performs insufficient input sanitising,
which permits an attacker to execute arbitrary FTP commands. This
requires the attacker to already have access to the FTP server.

For the oldstable distribution (sarge) this problem has been fixed
in version 4.3.10-21.

For the stable distribution (etch) this problem has been fixed
in version 4.4.4-8+etch3.

For the unstable distribution (sid) this problem won't be fixed, as php4
will be removed from sid. Thus you are strongly advised to migrate to
php5 if you prefer to follow the unstable distribution.");

  script_tag(name:"solution", value:"We recommend that you upgrade your PHP packages. Packages for the Sparc");
  script_tag(name:"summary", value:"The remote host is missing an update to php4
announced via advisory DSA 1296-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"php4-pear", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libapache-mod-php4", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libapache2-mod-php4", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-cgi", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-cli", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-common", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-curl", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-dev", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-domxml", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-gd", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-imap", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-ldap", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mcal", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mhash", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mysql", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-odbc", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-recode", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-snmp", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-sybase", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-xslt", ver:"4.3.10-21", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-pear", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libapache-mod-php4", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libapache2-mod-php4", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-cgi", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-cli", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-common", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-curl", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-dev", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-domxml", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-gd", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-imap", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-ldap", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mcal", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mcrypt", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mhash", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-mysql", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-odbc", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-pgsql", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-pspell", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-recode", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-snmp", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-sybase", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-xslt", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"php4-interbase", ver:"4.4.4-8+etch3", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
