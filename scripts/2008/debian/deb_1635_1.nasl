# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1635-1 (freetype)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61593");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-09-17 04:23:15 +0200 (Wed, 17 Sep 2008)");
  script_cve_id("CVE-2008-1806", "CVE-2008-1807", "CVE-2008-1808");
  script_tag(name:"cvss_base", value:"7.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1635-1 (freetype)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201635-1");
  script_tag(name:"insight", value:"Several local vulnerabilities have been discovered in freetype,
a FreeType 2 font engine, which could allow the execution of arbitrary
code.

The Common Vulnerabilities and Exposures project identifies the
following problems:

CVE-2008-1806
An integer overflow allows context-dependent attackers to execute
arbitrary code via a crafted set of values within the Private
dictionary table in a Printer Font Binary (PFB) file.

CVE-2008-1807
The handling of an invalid number of axes field in the PFB file could
trigger the freeing of arbitrary memory locations, leading to
memory corruption.

CVE-2008-1808
Multiple off-by-one errors allowed the execution of arbitrary code
via malformed tables in PFB files, or invalid SHC instructions in
TTF files.


For the stable distribution (etch), these problems have been fixed in version
2.2.1-5+etch3.

For the unstable distribution (sid), these problems have been fixed in
version 2.3.6-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your freetype package.");
  script_tag(name:"summary", value:"The remote host is missing an update to freetype
announced via advisory DSA 1635-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"freetype2-demos", ver:"2.2.1-5+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libfreetype6-dev", ver:"2.2.1-5+etch3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libfreetype6", ver:"2.2.1-5+etch3", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
