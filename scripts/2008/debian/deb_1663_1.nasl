# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1663-1 (net-snmp)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61853");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-11-19 16:52:57 +0100 (Wed, 19 Nov 2008)");
  script_cve_id("CVE-2008-0960", "CVE-2008-2292", "CVE-2008-4309");
  script_tag(name:"cvss_base", value:"10.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1663-1 (net-snmp)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201663-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in NET SNMP, a suite of
Simple Network Management Protocol applications. The Common
Vulnerabilities and Exposures project identifies the following problems:

CVE-2008-0960

Wes Hardaker reported that the SNMPv3 HMAC verification relies on
the client to specify the HMAC length, which allows spoofing of
authenticated SNMPv3 packets.

CVE-2008-2292

John Kortink reported a buffer overflow in the __snprint_value
function in snmp_get causing a denial of service and potentially
allowing the execution of arbitrary code via a large OCTETSTRING
in an attribute value pair (AVP).

CVE-2008-4309

It was reported that an integer overflow in the
netsnmp_create_subtree_cache function in agent/snmp_agent.c allows
remote attackers to cause a denial of service attack via a crafted
SNMP GETBULK request.

For the stable distribution (etch), these problems has been fixed in
version 5.2.3-7etch4.

For the testing distribution (lenny) and unstable distribution (sid)
these problems have been fixed in version 5.4.1~dfsg-11.");

  script_tag(name:"solution", value:"We recommend that you upgrade your net-snmp package.");
  script_tag(name:"summary", value:"The remote host is missing an update to net-snmp
announced via advisory DSA 1663-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libsnmp-base", ver:"5.2.3-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"tkmib", ver:"5.2.3-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libsnmp9-dev", ver:"5.2.3-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"snmp", ver:"5.2.3-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libsnmp9", ver:"5.2.3-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libsnmp-perl", ver:"5.2.3-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"snmpd", ver:"5.2.3-7etch4", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
