# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1421-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.59956");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:23:47 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2007-5742");
  script_tag(name:"cvss_base", value:"9.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:P/I:P/A:C");
  script_name("Debian Security Advisory DSA 1421-1 (wesnoth)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.1|4)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201421-1");
  script_tag(name:"insight", value:"A vulnerability has been discovered in Battle for Wesnoth that allows
remote attackers to read arbitrary files the user running the client
has access to on the machine running the game client.

For the old stable distribution (sarge) this problem has been fixed in
version 0.9.0-7.

For the stable distribution (etch) this problem has been fixed in
version 1.2-3.

For the stable backports distribution (etch-backports) this problem
has been fixed in version 1.2.8-1~bpo40+1.

For the unstable distribution (sid) this problem has been fixed in
version 1.2.8-1.

For the experimental distribution this problem has been fixed in
version 1.3.12-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your wesnoth package.");
  script_tag(name:"summary", value:"The remote host is missing an update to wesnoth
announced via advisory DSA 1421-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"wesnoth-data", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-ei", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-httt", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-music", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-sotbe", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-tdh", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-trow", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-editor", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-server", ver:"0.9.0-7", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-data", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-ei", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-httt", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-music", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-trow", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-tsg", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-ttb", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-utbs", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-editor", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"wesnoth-server", ver:"1.2-3", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
