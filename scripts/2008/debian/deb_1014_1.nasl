# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1014-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.56466");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:09:45 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2004-2043");
  script_bugtraq_id(10446);
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 1014-1 (firebird2)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB3\.1");
  script_tag(name:"solution", value:"For the stable distribution (sarge) this problem has been fixed in
version 1.5.1-4sarge1.

For the unstable distribution (sid) this problem has been fixed in
version 1.5.3.4870-3

  We recommend that you upgrade your firebird2 packages.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201014-1");
  script_tag(name:"summary", value:"The remote host is missing an update to firebird2
announced via advisory DSA 1014-1.

Aviram Jenik and Damyan Ivanov discovered a buffer overflow in
firebird2, an RDBMS based on InterBase 6.0 code, that allows remote
attackers to crash.

The old stable distribution (woody) does not contain firebird2 packages.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"firebird2-classic-server", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"firebird2-dev", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"firebird2-examples", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"firebird2-server-common", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"firebird2-super-server", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"firebird2-utils-classic", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"firebird2-utils-super", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libfirebird2-classic", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libfirebird2-super", ver:"1.5.1-4sarge1", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
