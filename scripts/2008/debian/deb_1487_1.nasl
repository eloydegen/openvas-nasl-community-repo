# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1487-1 (libexif)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.60360");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-02-15 23:29:21 +0100 (Fri, 15 Feb 2008)");
  script_cve_id("CVE-2007-2645", "CVE-2007-6351", "CVE-2007-6352");
  script_tag(name:"cvss_base", value:"9.3");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1487-1 (libexif)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.1|4)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201487-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in the EXIF parsing code
of the libexif library, which can lead to denial of service or the
xecution of arbitrary code if a user is tricked into opening a
malformed image.

CVE-2007-2645

Victor Stinner discovered an integer overflow, which may result in
denial of service or potentially the execution of arbitrary code.

CVE-2007-6351

Meder Kydyraliev discovered an infinite loop, which may result in
denial of service.

CVE-2007-6352

Victor Stinner discovered an integer overflow, which may result
in denial of service or potentially the execution of arbitrary
code.

This update also fixes two potential NULL pointer deferences.

For the stable distribution (etch), these problems have been fixed in
version 0.6.13-5etch2.

For the old stable distribution (sarge), these problems have been
fixed in 0.6.9-6sarge2.");

  script_tag(name:"solution", value:"We recommend that you upgrade your libexif packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to libexif
announced via advisory DSA 1487-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libexif-dev", ver:"0.6.9-6sarge2", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libexif10", ver:"0.6.9-6sarge2", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libexif12", ver:"0.6.13-5etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libexif-dev", ver:"0.6.13-5etch2", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
