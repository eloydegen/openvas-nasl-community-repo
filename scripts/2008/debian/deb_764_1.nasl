# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 764-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.54411");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:00:53 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2005-1524", "CVE-2005-1525", "CVE-2005-1526", "CVE-2005-2148", "CVE-2005-2149");
  script_tag(name:"cvss_base", value:"10.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 764-1 (cacti)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.0|3\.1)");
  script_tag(name:"solution", value:"For the stable distribution (sarge) these problems have been fixed in
version 0.8.6c-7sarge2.

For the unstable distribution (sid) these problems have been fixed in
version 0.8.6e-1.

  We recommend that you upgrade your cacti package.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20764-1");
  script_tag(name:"summary", value:"The remote host is missing an update to cacti
announced via advisory DSA 764-1.

Several vulnerabilities have been discovered in cacti, a round-robin
database (RRD) tool that helps create graphs from database
information.  The Common Vulnerabilities and Exposures Project
identifies the following problems:

CVE-2005-1524

Maciej Piotr Falkiewicz and an anonymous researcher discovered an
input validation bug that allows an attacker to include arbitrary
PHP code from remote sites which will allow the execution of
arbitrary code on the server running cacti.

CVE-2005-1525

Due to missing input validation cacti allows a remote attacker to
insert arbitrary SQL statements.

CVE-2005-1526

Maciej Piotr Falkiewicz discovered an input validation bug that
allows an attacker to include arbitrary PHP code from remote sites
which will allow the execution of arbitrary code on the server
running cacti.

CVE-2005-2148

Stefan Esser discovered that the update for the abovely mentioned
vulnerabilities does not perform proper input validation to
protect against common attacks.

CVE-2005-2149

Stefan Esser discovered that the update for CVE-2005-1525 allows
remote attackers to modify session information to gain privileges
and disable the use of addslashes to protect against SQL
injection.

For the old stable distribution (woody) these problems have been fixed in
version 0.6.7-2.5.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"cacti", ver:"0.6.7-2.5", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cacti", ver:"0.8.6c-7sarge2", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
