# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 960-3
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.56460");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:07:13 +0100 (Thu, 17 Jan 2008)");
  script_bugtraq_id(16434);
  script_cve_id("CVE-2005-4536");
  script_tag(name:"cvss_base", value:"2.1");
  script_tag(name:"cvss_base_vector", value:"AV:L/AC:L/Au:N/C:N/I:P/A:N");
  script_name("Debian Security Advisory DSA 960-3 (libmail-audit-perl)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.0|3\.1)");
  script_tag(name:"solution", value:"For the stable distribution (sarge) these problems have been fixed in
version 2.1-5sarge4.

For the unstable distribution (sid) these problems have been fixed in
version 2.1-5.1.

  We recommend that you upgrade your libmail-audit-perl package.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20960-3");
  script_tag(name:"summary", value:"The remote host is missing an update to libmail-audit-perl
announced via advisory DSA 960-3.

The former update caused temporary files to be created in the current
working directory due to a wrong function argument.  This update will
create temporary files in the users home directory if HOME is set or
in the common temporary directory otherwise, usually /tmp.  For
completeness below is a copy of the original advisory text:

Niko Tyni discovered that the Mail::Audit module, a Perl library
for creating simple mail filters, logs to a temporary file with a
predictable filename in an insecure fashion when logging is turned
on, which is not the case by default.

For the old stable distribution (woody) these problems have been fixed in
version 2.0-4woody3.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libmail-audit-perl", ver:"2.0-4woody3", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mail-audit-tools", ver:"2.0-4woody3", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libmail-audit-perl", ver:"2.1-5sarge4", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"mail-audit-tools", ver:"2.1-5sarge4", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
