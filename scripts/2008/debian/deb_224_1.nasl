# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 224-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.53720");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 22:28:10 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2002-1158", "CVE-2002-1159");
  script_bugtraq_id(6351, 6354);
  script_tag(name:"cvss_base", value:"7.2");
  script_tag(name:"cvss_base_vector", value:"AV:L/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 224-1 (canna)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(2\.2|3\.0)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20224-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in canna, a Japanese
input system.  The Common Vulnerabilities and Exposures (CVE) project
identified the following vulnerabilities:

  * CVE-2002-1158 (BugTraq Id 6351): hsj of Shadow Penguin Security
discovered a heap overflow vulnerability in the irw_through
function in canna server.

  * CVE-2002-1159 (BugTraq Id 6354): Shinra Aida of the Canna project
discovered that canna does not properly validate requests, which
allows remote attackers to cause a denial of service or information
leak.

For the current stable distribution (woody) these problems have been
fixed in version 3.5b2-46.2.

For the old stable distribution (potato) these problems have been
fixed in version 3.5b2-25.2.

For the unstable distribution (sid) these problems have been fixed in
version 3.6p1-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your canna packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to canna
announced via advisory DSA 224-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"canna", ver:"3.5b2-25.2", rls:"DEB2.2")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"canna-utils", ver:"3.5b2-25.2", rls:"DEB2.2")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcanna1g", ver:"3.5b2-25.2", rls:"DEB2.2")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcanna1g-dev", ver:"3.5b2-25.2", rls:"DEB2.2")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"canna", ver:"3.5b2-46.2", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"canna-utils", ver:"3.5b2-46.2", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcanna1g", ver:"3.5b2-46.2", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcanna1g-dev", ver:"3.5b2-46.2", rls:"DEB3.0")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
