# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 465-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.53162");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 22:41:51 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2004-0079", "CVE-2004-0081");
  script_tag(name:"cvss_base", value:"5.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:P");
  script_name("Debian Security Advisory DSA 465-1 (openssl, openssl094, openssl095)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB3\.0");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20465-1");
  script_xref(name:"URL", value:"http://www.uniras.gov.uk/vuls/2004/224012/index.htm");
  script_xref(name:"URL", value:"http://www.openssl.org/news/secadv_20040317.txt");
  script_tag(name:"insight", value:"Two vulnerabilities were discovered in openssl, an implementation of
the SSL protocol, using the Codenomicon TLS Test Tool.  More
information can be found in referenced NISCC Vulnerability
Advisory and OpenSSL advisory.

  - CVE-2004-0079 - null-pointer assignment in the
do_change_cipher_spec() function.  A remote attacker could perform
a carefully crafted SSL/TLS handshake against a server that used
the OpenSSL library in such a way as to cause OpenSSL to crash.
Depending on the application this could lead to a denial of
service.

  - CVE-2004-0081 - a bug in older versions of OpenSSL 0.9.6 that
can lead to a Denial of Service attack (infinite loop).

For the stable distribution (woody) these problems have been fixed in
openssl version 0.9.6c-2.woody.6, openssl094 version 0.9.4-6.woody.4
and openssl095 version 0.9.5a-6.woody.5.

For the unstable distribution (sid) these problems will be fixed soon.

We recommend that you update your openssl package.");
  script_tag(name:"summary", value:"The remote host is missing an update to openssl, openssl094, openssl095
announced via advisory DSA 465-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution", value:"Please install the updated package(s).");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"ssleay", ver:"0.9.6c-2.woody.6", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libssl-dev", ver:"0.9.6c-2.woody.6", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libssl0.9.6", ver:"0.9.6c-2.woody.6", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"openssl", ver:"0.9.6c-2.woody.6", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libssl095a", ver:"0.9.5a-6.woody.5", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libssl09", ver:"0.9.4-6.woody.3", rls:"DEB3.0")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
