# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1613-1 (libgd2)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61363");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-08-15 15:52:52 +0200 (Fri, 15 Aug 2008)");
  script_cve_id("CVE-2007-3476", "CVE-2007-3477", "CVE-2007-3996", "CVE-2007-2445");
  script_tag(name:"cvss_base", value:"6.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:P/I:P/A:P");
  script_name("Debian Security Advisory DSA 1613-1 (libgd2)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201613-1");
  script_tag(name:"insight", value:"Multiple vulnerabilities have been identified in libgd2, a library
for programmatic graphics creation and manipulation.  The Common
Vulnerabilities and Exposures project identifies the following three
issues:

CVE-2007-2445

Grayscale PNG files containing invalid tRNS chunk CRC values
could cause a denial of service (crash), if a maliciously
crafted image is loaded into an application using libgd.

CVE-2007-3476

An array indexing error in libgd's GIF handling could induce a
denial of service (crash with heap corruption) if exceptionally
large color index values are supplied in a maliciously crafted
GIF image file.

CVE-2007-3477

The imagearc() and imagefilledarc() routines in libgd allow
an attacker in control of the parameters used to specify
the degrees of arc for those drawing functions to perform
a denial of service attack (excessive CPU consumption).

CVE-2007-3996

Multiple integer overflows exist in libgd's image resizing and
creation routines. These weaknesses allow an attacker in control
of the parameters passed to those routines to induce a crash or
execute arbitrary code with the privileges of the user running
an application or interpreter linked against libgd2.

For the stable distribution (etch), these problems have been fixed in
version 2.0.33-5.2etch1.  For the unstable distribution (sid), the
problem has been fixed in version 2.0.35.dfsg-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your libgd2 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to libgd2
announced via advisory DSA 1613-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libgd2-xpm-dev", ver:"2.0.33-5.2etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgd-tools", ver:"2.0.33-5.2etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgd2-xpm", ver:"2.0.33-5.2etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgd2-noxpm", ver:"2.0.33-5.2etch1", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgd2-noxpm-dev", ver:"2.0.33-5.2etch1", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
