# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1640-1 (python-django)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61642");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-09-24 17:42:31 +0200 (Wed, 24 Sep 2008)");
  script_cve_id("CVE-2008-3909", "CVE-2007-5712");
  script_tag(name:"cvss_base", value:"5.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:N/C:N/I:P/A:P");
  script_name("Debian Security Advisory DSA 1640-1 (python-django)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201640-1");
  script_tag(name:"insight", value:"Simon Willison discovered that in Django, a Python web framework, the
feature to retain HTTP POST data during user reauthentication allowed
a remote attacker to perform unauthorized modification of data through
cross site request forgery. The is possible regardless of the Django
plugin to prevent cross site request forgery being enabled. The Common
Vulnerabilities and Exposures project identifies this issue as
CVE-2008-3909.

In this update the affected feature is disabled. This is in accordance
with upstream's preferred solution for this situation.

This update takes the opportunity to also include a relatively minor
denial of service attack in the internationalisaton framework, known
as CVE-2007-5712.

For the stable distribution (etch), these problems have been fixed in
version 0.95.1-1etch2.

For the unstable distribution (sid), these problems have been fixed in
version 1.0-1.");

  script_tag(name:"solution", value:"We recommend that you upgrade your python-django package.");
  script_tag(name:"summary", value:"The remote host is missing an update to python-django
announced via advisory DSA 1640-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"python-django", ver:"0.95.1-1etch2", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
