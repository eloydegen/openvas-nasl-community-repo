# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 967-1
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.56252");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 23:07:13 +0100 (Thu, 17 Jan 2008)");
  script_cve_id("CVE-2006-4439", "CVE-2006-0347", "CVE-2006-0348", "CVE-2006-0597", "CVE-2006-0598", "CVE-2006-0599", "CVE-2006-0600", "CVE-2005-4439");
  script_tag(name:"cvss_base", value:"7.8");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:C");
  script_name("Debian Security Advisory DSA 967-1 (elog)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB3\.1");
  script_tag(name:"solution", value:"For the stable distribution (sarge) these problems have been fixed in
version 2.5.7+r1558-4+sarge2.

For the unstable distribution (sid) these problems have been fixed in
version 2.6.1+r1642-1.

  We recommend that you upgrade your elog package.");

  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20967-1");
  script_tag(name:"summary", value:"The remote host is missing an update to elog
announced via advisory DSA 967-1.

Several security problems have been found in elog, an electonic logbook
to manage notes.  The Common Vulnerabilities and Exposures Project
identifies the following problems:

CVE-2005-4439

GroundZero Security discovered that elog insufficiently checks the
size of a buffer used for processing URL parameters, which might lead
to the execution of arbitrary code.

CVE-2006-0347

It was discovered that elog contains a directory traversal vulnerability
in the processing of ../ sequences in URLs, which might lead to
information disclosure.

CVE-2006-0348

The code to write the log file contained a format string vulnerability,
which might lead to the execution of arbitrary code.

CVE-2006-0597

Overly long revision attributes might trigger a crash due to a buffer
overflow.

CVE-2006-0598

The code to write the log file does not enforce bounds checks properly,
which might lead to the execution of arbitrary code.

CVE-2006-0599

elog emitted different errors messages for invalid passwords and invalid
users, which allows an attacker to probe for valid user names.

CVE-2006-0600

An attacker could be driven into infinite redirection with a crafted
fail request, which has denial of service potential.

The old stable distribution (woody) does not contain elog packages.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"elog", ver:"2.5.7+r1558-4+sarge2", rls:"DEB3.1")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
