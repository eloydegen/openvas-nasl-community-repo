# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1612-1 (ruby1.8)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61362");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-08-15 15:52:52 +0200 (Fri, 15 Aug 2008)");
  script_cve_id("CVE-2008-2662", "CVE-2008-2663", "CVE-2008-2664", "CVE-2008-2725", "CVE-2008-2726", "CVE-2008-2376", "CVE-2006-2662");
  script_tag(name:"cvss_base", value:"10.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1612-1 (ruby1.8)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201612-1");
  script_tag(name:"insight", value:"Several vulnerabilities have been discovered in the interpreter for
the Ruby language, which may lead to denial of service or the
execution of arbitrary code. The Common Vulnerabilities and Exposures
project identifies the following problems:

CVE-2006-2662

Drew Yao discovered that multiple integer overflows in the string
processing code may lead to denial of service and potentially the
execution of arbitrary code.

CVE-2008-2663

Drew Yao discovered that multiple integer overflows in the string
processing code may lead to denial of service and potentially the
execution of arbitrary code.

CVE-2008-2664

Drew Yao discovered that a programming error in the string
processing code may lead to denial of service and potentially the
execution of arbitrary code.

CVE-2008-2725

Drew Yao discovered that an integer overflow in the array handling
code may lead to denial of service and potentially the execution
of arbitrary code.

CVE-2008-2726

Drew Yao discovered that an integer overflow in the array handling
code may lead to denial of service and potentially the execution
of arbitrary code.

CVE-2008-2376

It was discovered that an integer overflow in the array handling
code may lead to denial of service and potentially the execution
of arbitrary code.

For the stable distribution (etch), these problems have been fixed in
version 1.8.5-4etch2.

For the unstable distribution (sid), these problems have been fixed in
version 1.8.7.22-2.");

  script_tag(name:"solution", value:"We recommend that you upgrade your ruby1.8 packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to ruby1.8
announced via advisory DSA 1612-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"rdoc1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ruby1.8-elisp", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"irb1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ruby1.8-examples", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ri1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libopenssl-ruby1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libreadline-ruby1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libgdbm-ruby1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libruby1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libdbm-ruby1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libruby1.8-dbg", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libtcltk-ruby1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ruby1.8-dev", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"ruby1.8", ver:"1.8.5-4etch2", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
