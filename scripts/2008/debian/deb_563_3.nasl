# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 563-3
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Text descriptions are largerly excerpted from the referenced
# advisory, and are Copyright (c) the respective author(s)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.53256");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-17 22:45:44 +0100 (Thu, 17 Jan 2008)");
  script_bugtraq_id(11347);
  script_cve_id("CVE-2004-0884");
  script_tag(name:"cvss_base", value:"7.2");
  script_tag(name:"cvss_base_vector", value:"AV:L/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 563-3 (cyrus-sasl)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB3\.0");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%20563-3");
  script_tag(name:"insight", value:"This advisory is an addition to DSA 563-1 and 563-2 which weren't able
to supersede the library on sparc and arm due to a different version
number for them in the stable archive.  Other architectures were
updated properly.  Another problem was reported in connection with
sendmail, though, which should be fixed with this update as well.

For the stable distribution (woody) this problem has been fixed in
version 1.5.27-3.1woody5.

For reference the advisory text follows:

A vulnerability has been discovered in the Cyrus implementation of
the SASL library, the Simple Authentication and Security Layer, a
method for adding authentication support to connection-based
protocols.  The library honors the environment variable SASL_PATH
blindly, which allows a local user to link against a malicious
library to run arbitrary code with the privileges of a setuid or
setgid application.

For the unstable distribution (sid) this problem has been fixed in
version 1.5.28-6.2 of cyrus-sasl and in version 2.1.19-1.3 of
cyrus-sasl2.");

  script_tag(name:"solution", value:"We recommend that you upgrade your libsasl packages.");
  script_tag(name:"summary", value:"The remote host is missing an update to cyrus-sasl
announced via advisory DSA 563-3.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libsasl-dev", ver:"1.5.27-3.1woody5", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libsasl-digestmd5-plain", ver:"1.5.27-3.1woody5", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libsasl-modules-plain", ver:"1.5.27-3.1woody5", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libsasl7", ver:"1.5.27-3.1woody5", rls:"DEB3.0")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"sasl-bin", ver:"1.5.27-3.1woody5", rls:"DEB3.0")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
