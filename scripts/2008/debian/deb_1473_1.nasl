# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1473-1 (scponly)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.60217");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-01-31 16:11:48 +0100 (Thu, 31 Jan 2008)");
  script_cve_id("CVE-2007-6350", "CVE-2007-6415");
  script_tag(name:"cvss_base", value:"8.5");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:M/Au:S/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1473-1 (scponly)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB(3\.1|4)");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201473-1");
  script_tag(name:"insight", value:"Joachim Breitner discovered that Subversion support in scponly is
inherently insecure, allowing execution of arbitrary commands.  Further
investigation showed that rsync and Unison support suffer from similar
issues.  This set of issues has been assigned CVE-2007-6350.

In addition, it was discovered that it was possible to invoke with scp
with certain options that may lead to execution of arbitrary commands
(CVE-2007-6415).

This update removes Subversion, rsync and Unison support from the
scponly package, and prevents scp from being invoked with the dangerous
options.

For the stable distribution (etch), these problems have been fixed in
version 4.6-1etch1.

For the old stable distribution (sarge), these problems have been fixed
in version 4.0-1sarge2.

The unstable distribution (sid) will be fixed soon.");

  script_tag(name:"solution", value:"We recommend that you upgrade your scponly package.");
  script_tag(name:"summary", value:"The remote host is missing an update to scponly
announced via advisory DSA 1473-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"scponly", ver:"4.0-1sarge2", rls:"DEB3.1")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"scponly", ver:"4.6-1etch1", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
