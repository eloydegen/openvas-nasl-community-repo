# OpenVAS Vulnerability Test
# Description: Auto-generated from advisory DSA 1625-1 (cupsys)
#
# Authors:
# Thomas Reinke <reinke@securityspace.com>
#
# Copyright:
# Copyright (C) 2008 E-Soft Inc.
# Some text descriptions might be excerpted from (a) referenced
# source(s), and are Copyright (C) by the respective right holder(s).
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2,
# as published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#

if(description)
{
  script_oid("1.3.6.1.4.1.25623.1.0.61377");
  script_version("2022-01-13T13:47:22+0000");
  script_tag(name:"last_modification", value:"2022-01-13 13:47:22 +0000 (Thu, 13 Jan 2022)");
  script_tag(name:"creation_date", value:"2008-08-15 15:52:52 +0200 (Fri, 15 Aug 2008)");
  script_cve_id("CVE-2008-0053", "CVE-2008-1373", "CVE-2008-1722");
  script_tag(name:"cvss_base", value:"10.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:C/A:C");
  script_name("Debian Security Advisory DSA 1625-1 (cupsys)");
  script_category(ACT_GATHER_INFO);
  script_copyright("Copyright (C) 2008 E-Soft Inc.");
  script_family("Debian Local Security Checks");
  script_dependencies("gather-package-list.nasl");
  script_mandatory_keys("ssh/login/debian_linux", "ssh/login/packages", re:"ssh/login/release=DEB4");
  script_xref(name:"URL", value:"https://secure1.securityspace.com/smysecure/catid.html?in=DSA%201625-1");
  script_tag(name:"insight", value:"Several remote vulnerabilities have been discovered in the Common Unix
Printing System (CUPS). The Common Vulnerabilities and Exposures project
identifies the following problems:

CVE-2008-0053

Buffer overflows in the HP-GL input filter allowed to possibly run
arbitrary code through crafted HP-GL files.

CVE-2008-1373

Buffer overflow in the GIF filter allowed to possibly run arbitrary
code through crafted GIF files.

CVE-2008-1722

Integer overflows in the PNG filter allowed to possibly run arbitrary
code through crafted PNG files.

For the stable distribution (etch), these problems have been fixed in
version 1.2.7-4etch4 of package cupsys.

For the testing (lenny) and unstable distribution (sid), these problems
have been fixed in version 1.3.7-2 of package cups.");

  script_tag(name:"solution", value:"We recommend that you upgrade your cupsys package.");
  script_tag(name:"summary", value:"The remote host is missing an update to cupsys
announced via advisory DSA 1625-1.");
  script_tag(name:"qod_type", value:"package");
  script_tag(name:"solution_type", value:"VendorFix");

  exit(0);
}

include("revisions-lib.inc");
include("pkg-lib-deb.inc");

res = "";
report = "";
if((res = isdpkgvuln(pkg:"libcupsys2-gnutls10", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-common", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-dbg", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsimage2-dev", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsys2-dev", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-client", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsimage2", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"cupsys-bsd", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}
if((res = isdpkgvuln(pkg:"libcupsys2", ver:"1.2.7-4etch4", rls:"DEB4")) != NULL) {
  report += res;
}

if(report != "") {
  security_message(data:report);
} else if(__pkg_match) {
  exit(99);
}
